
#ifndef PLAYER
#define PLAYER

#include <string>
using namespace std;

class Player{
public:
	Player(char*, char*);
	Player(const Player&);
	Player();
	char* getName();
	char* getPass();
	void addWin();
	void addLoss();
	void addTie();

	int getTies();
	int getWins();
	int getLosses();

	bool operator==(Player);
	bool operator!=(Player);
	bool validPass(Player);

	bool operator<(const Player &);
	bool operator>(const Player &);
private:

	char* userName;
	char* password;

	int wins;
	int losses;
	int ties; 

	int socket; //socket this player belongs too
};

#endif