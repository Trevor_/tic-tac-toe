// T3h Victor & Trevor
// Final Project
// file client.cpp
#include <sys/poll.h>     // for poll( )
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <sstream>
#include <strings.h>
#include <stdlib.h>
#include <string>
#include <pthread.h>
#include <sys/time.h>
#include <vector> 
#include "player.h"
#include "tictactoe.h"
#include <mutex>
#include <cstdio>
#include <ctime> 
#include <algorithm>

#define MAXUSERNAMELENGTH 31
#define MAXPASSWORDLENGTH 34
#define MAXONLINESESSIONS 100
#define MAXPLAYERS 100
#define MAXGAMES 50
#define SCOREBOARDLIMIT 5
#define SIZEOFMESSAGE 201

struct Carrier{
	int parentThreadId;
	char *message;
	int socket;
};

//MUST BE MUTEX LOCKED
struct SessionID{
	char *username;
	int socketNum;
	int gameID;
};

struct Message{
  char* message;
  int sock;
};

double KEEPALIVE = 12;
char PORT[] = "8282";
int G_PLAYER1 = 1;
int G_PLAYER2 = 2;
int BUFSIZE = 1500;
int request_limit = 65; // max inbound connection

int MAXGAMESAVAILWRITEBACK = 10;

//modules
int REGISTER_MODULE = 1;
int LOGIN_MODULE = 2;
int LIST_GAME_MODULE = 3;
int CREATE_GAME_MODULE = 4;
int SCOREBOARD_MODULE = 5;
int JOIN_GAME_MODULE = 6;
int LOGOUT_MODULE = 7;
int DELETE_MODULE = 8;
int HOST_POLL_MODULE = 9;
int KEEP_ALIVE_MODULE = 10;
int POLL_GAME_STATUS_MODULE = 11;
int MAKE_MOVE_MODULE = 12;
int QUIT_GAME_MODULE = 13;
int SEND_MESSAGE_MODULE = 14;
int POLL_MESSAGE_MODULE = 15;

//INITALIZATION OF SHARED DATA OBJECTSx 
//data structures for server
vector<TicTacToe*> gamesInsession;
pthread_mutex_t gamesInsessionMTX;
vector<TicTacToe*> gamesAvail;
pthread_mutex_t  gamesAvailMTX;
vector<Player*> players;
pthread_mutex_t playersMTX;

//scoreboard stuff
vector<Player> scoreBoardVect;
pthread_mutex_t scoreBoardVectMTX;

//online session vector
vector<SessionID> onlineSessions;
pthread_mutex_t onlineSessionMTX;

//message vector
vector<Message> messageVect;
pthread_mutex_t messageMTX;

//END INITALIZATION


int G_GAMEID = 0;

void *driver(void*);
void *registerClient(void*);
void *loginClient(void*);
void *listGame(void*);
void *createGame(void*);
void *scoreBoard(void*);
void *joinGame(void*);
void *logout(void*);
void *deletePlayer(void*);
void *hostModulePoll(void*);
void *gameStatusModule(void*);
void *makeMove(void*);
void *quitGame(void*);
void *sendMessage(void*);
void *pollMessage(void*);

//runs the server, accepting connections from the client
int main(int argc, char* argv[])
{
  //mutex init
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);
  pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
  
  pthread_mutex_init(&gamesInsessionMTX, &attr);
  pthread_mutex_init(&gamesAvailMTX, &attr);
  pthread_mutex_init(&scoreBoardVectMTX, &attr);
  pthread_mutex_init(&onlineSessionMTX, &attr);  
  pthread_mutex_init(&playersMTX, &attr);
  pthread_mutex_init(&messageMTX, &attr);
  
  pthread_t thread_id;
  
  int sockfd;
  int yes = 1; // for allowing port reuse
  struct addrinfo *servinfo;  // will point to the results
  struct addrinfo *pointer;   // points to current location
  struct sockaddr_storage their_addr; // stores connecting client info
  socklen_t addr_size;
  char s[INET6_ADDRSTRLEN]; // max size of s will be an ipv6 address
  
  struct addrinfo hints;
  memset(&hints, 0, sizeof hints); // zeros the object
  hints.ai_family = AF_UNSPEC;     // don't care ipv4/6
  hints.ai_socktype = SOCK_STREAM; // TCP stream socket
  hints.ai_flags = AI_PASSIVE;     // fills in IP
	
  int status;
  if ((status = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
    perror("Server: error getting address info ");
    exit(1);
  }
  
  // loop through all the results and bind to the first we can
  for (pointer = servinfo; pointer != NULL; pointer = pointer->ai_next) {
    if ((sockfd = socket(pointer->ai_family, pointer->ai_socktype, pointer->ai_protocol)) == -1) {
      // couldn't open socket, will try next one
      continue;
    }
    
		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		  // couldn't set socket options, closing and retrying next poinrwe
		  close(sockfd);
		  continue;
		}
		
		if (::bind(sockfd, pointer->ai_addr, pointer->ai_addrlen) == -1) {
		  // couldn't bind to socket
		  close(sockfd);
		  continue;
		}
		break;
  }
  // if pointer is null, then hit end of list without opening/binding socket
  if (pointer == NULL) {
    cerr <<  "Server: failed to bind socket" << endl;
		exit(1);
  }
  
  freeaddrinfo(servinfo); // frees the list
  
  if (listen(sockfd, request_limit) == -1) {
    perror("Server: error listening on socket ");
    close(sockfd);
    exit(1);
  }
  
  
  
  //infinite loop, accepting connections and then creating new threads
  //to run the task function 
  while (int sock = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size)){
    if(pthread_create(&thread_id, NULL, &driver, (void*) &sock)<0){
      perror("could not create thread");
    }
  }
  
}

//thread that runs the connection between the server and the client
//the client will send a module number, and the server
//will create a thread for it 
void *driver(void * socket_desc){
  using namespace std;
  
  pthread_t thread_id;
  int size;
  int sock = *(int * )socket_desc;

  timeval time; 
  gettimeofday(&time, NULL);
  double timer = time.tv_sec;
  double currentTime = time.tv_sec;

  while( currentTime - timer < KEEPALIVE){//will be calling the time out

    
    int bytes_available = 0;
    // cout << "attempt to unlock 1: " << pthread_mutex_unlock(&ConnectionLock) << endl;
    Carrier *carrier = new Carrier();
    carrier->parentThreadId = pthread_self();
    carrier->socket = sock;
    char *dataBuf= new char[1500];
    carrier->message = dataBuf;


    //sets up polling of buffer
    struct pollfd pfd[1];
    pfd[0].fd = sock;             // declare I'll check the data availability of sd
    pfd[0].events = POLLRDNORM; // declare I'm interested in only reading from sd
    
    // check it immediately and return a positive number if sd is readable
    // otherwise it will sleep for 3000us
    int pollStatus;
    while ( pollStatus = poll( pfd, 1, 0 ) < 1) {  
      if ( pollStatus == -1) {
	pthread_exit(0);
      }
      usleep(10000); 
    }
    
    
    // reads in length of second message
    for (int nRead = 0; ( nRead += read( sock, (char*)&size, (sizeof(int) - nRead) ) ) < sizeof(int);) {
			if (nRead == -1) {
			
			  close(sock);
			  pthread_exit(0); // DONT FORGET TO END CHILD THREADS TREVOR
			}
			while ( pollStatus = poll( pfd, 1, 0 ) < 1) {
			  if ( pollStatus == -1) {
			    pthread_exit(0);
			  }
			  usleep(10000);
			}

    }
    gettimeofday(&time, NULL);
 	timer = time.tv_sec;
    currentTime = time.tv_sec;



    while ( pollStatus = poll( pfd, 1, 0 ) < 1) {
      if ( pollStatus == -1) {
	pthread_exit(0);
      }
      usleep(10000);
    }
	
    for (int nRead = 0; ( nRead += read( sock, dataBuf, size - nRead ) ) < size;) {
      if (nRead == -1) {

	close(sock);
	pthread_exit(0); 
      }
      
      while ( pollStatus = poll( pfd, 1, 0 ) < 1) {
	if ( pollStatus == -1) {
	  pthread_exit(0);
	}
	usleep(10000);
      }

      
    }
    
   //if statements to decide which thread to run, based on the module
    //sent from the user
    if((int )dataBuf[0] == REGISTER_MODULE){

      if(pthread_create(&thread_id, NULL, registerClient, (void*) carrier)<0){
	perror("could not create thread");
      }
 
      cout << pthread_join(thread_id, NULL) << endl;

    }
    else if((int)dataBuf[0] == LOGIN_MODULE){

     

      if(pthread_create(&thread_id, NULL, loginClient, (void*) carrier)<0){
	perror("could not create thread");
   

      }

      pthread_join(thread_id, NULL);
  
    }
    else if((int)dataBuf[0] == LIST_GAME_MODULE){
      
      if(pthread_create(&thread_id, NULL, listGame, (void*) carrier)<0){
	perror("could not create thread");
    

      }

      pthread_join(thread_id, NULL);

    }
    else if((int)dataBuf[0] == CREATE_GAME_MODULE){

      
      if(pthread_create(&thread_id, NULL, createGame, (void*) carrier)<0){
	perror("could not create thread");
 
      }
      
      pthread_join(thread_id, NULL);

    }
    else if((int)dataBuf[0] == SCOREBOARD_MODULE){

      if(pthread_create(&thread_id, NULL, scoreBoard, (void*) carrier)<0){
	perror("could not create thread");
      }
      
      pthread_join(thread_id, NULL);

    }
    else if((int)dataBuf[0] == JOIN_GAME_MODULE){
   
      if(pthread_create(&thread_id, NULL, joinGame, (void*) carrier)<0){
	perror("could not create thread");

      }

      pthread_join(thread_id, NULL);

    }	
    else if((int)dataBuf[0] == LOGOUT_MODULE){

      if(pthread_create(&thread_id, NULL, logout, (void*) carrier)<0){
	perror("could not create thread");

      }
     
      pthread_join(thread_id, NULL);

    }
        else if((int)dataBuf[0] == DELETE_MODULE){

      if(pthread_create(&thread_id, NULL, deletePlayer, (void*) carrier)<0){
	perror("could not create thread");

      }

      pthread_join(thread_id, NULL);

    }	
    else if((int)dataBuf[0] == HOST_POLL_MODULE){

      if(pthread_create(&thread_id, NULL, hostModulePoll, (void*) carrier)<0){
	perror("could not create thread");

      }

      pthread_join(thread_id, NULL);

    }	  
    else if((int)dataBuf[0] == POLL_GAME_STATUS_MODULE){
   
      if(pthread_create(&thread_id, NULL, gameStatusModule, (void*) carrier)<0){
	perror("could not create thread");

      }

      pthread_join(thread_id, NULL);
    }  
    else if((int)dataBuf[0] == MAKE_MOVE_MODULE){

      if(pthread_create(&thread_id, NULL, makeMove, (void*) carrier)<0){
	perror("could not create thread");

      }

      pthread_join(thread_id, NULL);

    }
    else if((int)dataBuf[0] == QUIT_GAME_MODULE){

      if(pthread_create(&thread_id, NULL, quitGame, (void*) carrier)<0){
  perror("could not create thread");

      }
     
      pthread_join(thread_id, NULL);

    }
    else if((int)dataBuf[0] == SEND_MESSAGE_MODULE){
  
      if(pthread_create(&thread_id, NULL, sendMessage, (void*) carrier)<0){
  perror("could not create thread");

      }

      pthread_join(thread_id, NULL);

    }
    else if((int)dataBuf[0] == POLL_MESSAGE_MODULE){
 
      if(pthread_create(&thread_id, NULL, pollMessage, (void*) carrier)<0){
        perror("could not create thread");
  
      }
      pthread_join(thread_id, NULL);

    }
    else if((int)dataBuf[0] == KEEP_ALIVE_MODULE){
    	//double countDown = time.tv_sec;
     	//already done
    }   
  }	

  Carrier *end = new Carrier();
  end->socket = sock;
  if(pthread_create(&thread_id, NULL, logout, (void*) end)<0){
    perror("could not create thread");
  }

  pthread_join(thread_id, NULL);
  pthread_exit(0);
}



//thread to register a new user to the tic tac toe game 
//client send (int length username, string username, int length pass, string password)
//the password is hashed on the client
void *registerClient (void * info){
	using namespace std;
	
	//lock the player vector!
	pthread_mutex_lock(&playersMTX);

	int userNameLength = ((Carrier*)info)->message[sizeof(int)];
	int userNamePos = sizeof(int)*2;
	int passwordLength = ((Carrier*)info)->message[userNameLength+userNamePos+1];
	int passwordPos = sizeof(int)+userNameLength+userNamePos+1;
	char *userName = new char[userNameLength]; 
	char *pass = new char[passwordLength]; 
	int addClient = 1; //true intially because we can add client always, turns false if the client exists
 
	//gets username and password from data buffer
	memcpy(userName, ((Carrier*)info)->message+userNamePos, userNameLength);
	memcpy(pass, ((Carrier*)info)->message+passwordPos, passwordLength);
	
	Player *temp = new Player(userName, pass);
	//loop through the player array
	for(int i = 0; i < players.size(); i++){

		if(strcmp(players[i]->getName(),userName)==0){ 
			addClient = 0; //player can not be added
			delete temp; //done with temp because the userName already exists
			break;
		}
	}

	//never found an player with the same username
	if(addClient){
		//insert player in the front of array to access quicker. this user is most likely the next player we need to access
		players.push_back(temp); //add the player to the back of the vector

	}

	//write the information bac to the client
	//true == added false == notAdded, player excists already, or no more new Players can be added
	for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&addClient, sizeof(addClient)) < sizeof(addClient);){
		if(written == -1){

			pthread_mutex_unlock(&playersMTX);

			delete[] ((Carrier*)info)->message;
			delete[] ((Carrier*)info);
			pthread_exit(0);
		}
	}


	//unlock the player vector!!
	pthread_mutex_unlock(&playersMTX);

	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);
	pthread_exit(0);
}

//thread to login 
//client will send (int username length, string username, int password length, string password)
//the password is hashed on the client
void *loginClient(void * info){
	using namespace std;

	//if its empty, exit right away
	if(players.empty()){
		int failed = 0;
		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&failed, sizeof(failed)) < sizeof(failed);){
			if(written == -1){

				delete[] ((Carrier*)info)->message;
				delete[] ((Carrier*)info);
				pthread_exit(0);
			}
		}
		
		delete[] ((Carrier*)info)->message;
		delete[] ((Carrier*)info);
		
		pthread_exit(0);
	}

	//lock the player vector!
	pthread_mutex_lock(&playersMTX);

	int userNameLength = ((Carrier*)info)->message[sizeof(int)];
	int userNamePos = sizeof(int)*2;
	int passwordLength = ((Carrier*)info)->message[userNameLength+userNamePos+1];
	int passwordPos = sizeof(int)+userNameLength+userNamePos+1;
	char *userName = new char[userNameLength];
	char *pass = new char[passwordLength];
	bool foundClient = false;

	
	//gets username and password from data buffer
	memcpy(userName, ((Carrier*)info)->message+userNamePos, userNameLength);
	memcpy(pass, ((Carrier*)info)->message+passwordPos, passwordLength);
	
	bool validClient = false; //is the username/pass valid
	int writeBack = 0; //value to be writen to the client, start at 0 because userName has not yet been found
	

	//loop through the player array
	for(int i = 0; i < players.size(); i++){
		//there is already a player with that username
		if(strcmp(players[i]->getName(),userName) == 0 && strcmp(players[i]->getPass(),pass) == 0){ 

			validClient = true; //we got it!
			writeBack = 1; //we found it!
			break; //break out of the loop because we found the client
		}
		else if(strcmp(players[i]->getName(),userName) == 0){
			writeBack = 2; //invalid pass
		}
	}
	//	cout << "after loop" << endl;

	bool addedToSession = false;
	//we found the client, now need to add it to the online array
	if(validClient){
		//lock the online season
		//int didWeLock;
	  pthread_mutex_lock(&onlineSessionMTX);
	  	  
	  SessionID newSes;
	  newSes.socketNum = ((Carrier*)info)->socket;
	  newSes.username = userName;
	  addedToSession = true;
	  onlineSessions.push_back(newSes);
	  pthread_mutex_unlock(&onlineSessionMTX);
	}
	
	if (addedToSession == false  && writeBack == 1) {

		writeBack = 3; //could not add to online array, so must be full
	}
	
	//(write to client) 1 = success, 0 = invalid username, 2 = bad password, 3 = onlinelist is full 
	for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&writeBack, sizeof(writeBack)) < sizeof(writeBack);){
		if(written == -1){

			cout << "invalid write login " << endl;


			//unlock the player vector!!
			pthread_mutex_unlock(&playersMTX);


			delete[] ((Carrier*)info)->message;
			delete[] ((Carrier*)info);

			pthread_exit(0);
		}
	}

	//unlock the player vector!!
	pthread_mutex_unlock(&playersMTX);

	

	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);

	pthread_exit(0);

}

//thread to send back the available games to the user (games with only one player)
//client will send list game module (int module)
//Server sned data format: (int gameIdNum, string username of other player, int wins, int losses)
//If no game, (-1)
void *listGame(void* info){

	using namespace std;
	char * userName; //to store the username
	int wins; //wins of user
	int losses; //losses of user
	int ties;

	//locking gamesAvail vector
	pthread_mutex_lock(&gamesAvailMTX);

 int numGamesAvail = gamesAvail.size();

 if(numGamesAvail == 0){

		numGamesAvail = -1;
		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&numGamesAvail, sizeof(numGamesAvail))<sizeof(numGamesAvail);) {

		  cout << "invalid write, number of games available" << endl;
		
		}
		pthread_mutex_unlock(&gamesAvailMTX);
		pthread_exit(0); //no games avail, exit thread
	}else if(numGamesAvail <= MAXGAMESAVAILWRITEBACK){
		//write back the number of games available
		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&numGamesAvail, sizeof(numGamesAvail))<sizeof(numGamesAvail);){
			if(written = -1){

				cout << "invalid write, number of games available" << endl;
				
        pthread_mutex_unlock(&gamesAvailMTX);				
				pthread_exit(0); //no games avail, exit thread
			
      }
		}
	}else{
		numGamesAvail = MAXGAMESAVAILWRITEBACK;
		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&numGamesAvail, sizeof(numGamesAvail))<sizeof(numGamesAvail);){
			if(written = -1){

				cout << "invalid write, number of games available" << endl;
				pthread_mutex_unlock(&gamesAvailMTX);

				pthread_exit(0); //no games avail, exit thread
			}
		}
	}

	for(int i = 0; i < gamesAvail.size() && i < MAXGAMESAVAILWRITEBACK; i++){
			int sizeOfUserName = strlen(gamesAvail[i]->getPlayer(G_PLAYER1))+1; //get the username of the player
			int sizeOfWriteBack = (sizeof(int) * 5) + sizeOfUserName; //calculate size of message

			for(int written = 0; written+= write(((Carrier*)info)->socket, (char *)&sizeOfWriteBack, sizeof(sizeOfWriteBack))<sizeof(sizeOfWriteBack);){
				if(written = -1){

					cout << "invalid write, size of writeBack" << endl;
					pthread_mutex_unlock(&gamesAvailMTX);

					pthread_exit(0); //no games avail, exit thread
				}
			} 
			//write back the size of the message
			char writeBack[sizeOfWriteBack]; //message back

			//create message for client in format:
			//(int gameIdNum, length of userName, string username of other player, int wins, int losses)
			writeBack[0] = gamesAvail[i]->getGameID(); //store the gameid
			writeBack[sizeof(int)] = sizeOfUserName; //store length of username


			memcpy(writeBack + sizeof(int)*2, gamesAvail[i]->getPlayer(G_PLAYER1), sizeOfUserName); //store user name for msg to client


			int temp = (sizeof(int)*2) + sizeOfUserName; //pos of wins int
			writeBack[temp] = gamesAvail[i]->getWins(G_PLAYER1); //store wins for writeback
			temp+=sizeof(int); //pos of losses int
			writeBack[temp] = gamesAvail[i]->getLosses(G_PLAYER1);//store losses for message to client
      temp+=sizeof(int); //pos of losses int
      writeBack[temp] = gamesAvail[i]->getTies(G_PLAYER1);//store losses for message to client

			for(int written = 0; written+=write(((Carrier*)info)->socket, (char *)writeBack, sizeof(writeBack)) < sizeof(writeBack);){
				if(written = -1){

					cout << "invalid write, writeing back the message" << endl;
					
					pthread_mutex_unlock(&gamesAvailMTX);


					pthread_exit(0); //no games avail, exit thread
				}
			} 
			//write message back to the server
			cout << "wrote " << gamesAvail[i]->getGameID() << " msg" << endl;
	}

	//unlocking gamesAvail Vector
	pthread_mutex_unlock(&gamesAvailMTX);

 cout << "done with listing available games" << endl;
 	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);
	pthread_exit(0);
}


//thread to create a new game for the client 
//Server: Creates a tic-tac-toe object, initializes all variables, sets p1 to this player, returns okay
//Server data format: (int gameIDNum); send -1 if unable to make game.
//Client side: then sits and polls host module…
//-------just creates a new game, does not add the player to a game if it is a available---------
void *createGame(void* info ){
	using namespace std;

	G_GAMEID++; //GET NEW GAME ID 
	TicTacToe *newGame = new TicTacToe();//creat new tictactoe object

	newGame->setGameID(G_GAMEID);//set the game id
	char* userName; //the user name of the current player
	bool foundPlayer = false;

	//lock the online sessions
	pthread_mutex_lock(&onlineSessionMTX);


	//find the userName
	for(int i = 0; i < onlineSessions.size(); i++){
		//if the socks are equal get the user name
		if(onlineSessions[i].socketNum == ((Carrier*)info)->socket){
			foundPlayer = true;
			userName = onlineSessions[i].username;
			break;
		}
	}

	//unlock the online session!!
	pthread_mutex_unlock(&onlineSessionMTX);
	if(foundPlayer){
		foundPlayer = false; //set back to not found
		//lock the player vector!
		pthread_mutex_lock(&playersMTX);

		//loop through and get the player
		for(int i = 0; i < players.size(); i++){
			if(strcmp(players[i]->getName(), userName) == 0){
				foundPlayer = true; //we found the guy

				newGame->addPlayer(players[i]);
        newGame->setSocket(G_PLAYER1,((Carrier*)info)->socket);
				gamesAvail.push_back(newGame);

				break;
			}
		}


		//unlock the player vector!!
		pthread_mutex_unlock(&playersMTX);

		
	}
	
	int writeback;
	if(foundPlayer){
		writeback = newGame->getGameID();
	}
	else{
		writeback = -1;
	}

	//write back to the server
	for(int written = 0; written+=write(((Carrier*)info)->socket, (char *)&writeback, sizeof(writeback)) < sizeof(writeback);){
		if(written == -1){
			//debug 
			cout << "fail writing back gameId, create new game" << endl;
		}	
	}

	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);
	pthread_exit(0);

}


//thread to write back the updated scoreboard module
//Client checks every 10 seconds for latest scoreboard
//Client sends: (<# for getscoreboard module>)
//Server: returns scoreboard of top 5 in format 
//write 1: (int number of writes)
// write 2: (int username length, username, win, loss) 
//write 3: username2, win2,loss2, etc)
// write 4: etc. 
//If less than 5, send (-1,-1,-1) for just that user that doesn’t exist, and client side must handle it by not showing those
void *scoreBoard(void* info){

  //lock the scoreboard  
  pthread_mutex_lock(&scoreBoardVectMTX);

	//empty out the scoreboard
	scoreBoardVect.clear();

	//copy top players into the score board
	for(int i = 0; i < SCOREBOARDLIMIT && i < players.size(); i++) {

	scoreBoardVect.push_back(*players[i]);
	}

	int numPlayers = scoreBoardVect.size();
	for(int written = 0; written+=write(((Carrier*)info)->socket, (char *)&numPlayers, sizeof(numPlayers))<sizeof(numPlayers);){
		if(written ==  -1){

		  pthread_mutex_unlock(&scoreBoardVectMTX);
		pthread_exit(0);
		}
	}
	
	//loop through the score board and create write buffer
	for(int gameId = 0; gameId < scoreBoardVect.size(); gameId++){

			int sizeOfUserName = strlen(scoreBoardVect[gameId].getName())+1; //get the username of the player
			int sizeOfWriteBack = (sizeof(int) * 4) + sizeOfUserName; //calculate size of message
			
			//write back the size of the message	
			for(int written = 0; written+=write(((Carrier*)info)->socket, (char *)&sizeOfWriteBack, sizeof(sizeOfWriteBack)) < sizeof(sizeOfWriteBack);){
				if(written ==  -1){
		
					pthread_mutex_unlock(&scoreBoardVectMTX);
					pthread_exit(0);
				}
			} 

			char writeBack[sizeOfWriteBack]; //message back
				
			//create message for client in format:
			//(length of userName, string username of other player, int wins, int losses)
			writeBack[0] = sizeOfUserName; //store length of username
			memcpy(writeBack + sizeof(int), scoreBoardVect[gameId].getName(), sizeOfUserName); //store user name for msg to client
			int temp = (sizeof(int)) + sizeOfUserName; //pos of wins int
			writeBack[temp] = scoreBoardVect[gameId].getWins(); //store wins for writeback

			temp+=sizeof(int); //pos of losses int
			writeBack[temp] = scoreBoardVect[gameId].getLosses();//store losses for message to client
      temp+=sizeof(int); //pos of losses int
      writeBack[temp] = scoreBoardVect[gameId].getTies();//store losses for message to client
			write(((Carrier*)info)->socket, (char *)writeBack, sizeof(writeBack)); //write message back to the server

	}

	//unlock the scoreboard!
	pthread_mutex_unlock(&scoreBoardVectMTX);

	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);
	pthread_exit(0);

}

//thread to join an already played game
//send back -1 if failed
//send back (size of next write) if it happend
//(int username lenght, other player username, int other player wins, other player int losses)
void *joinGame(void* info){

	int gameID = ((Carrier*)info)->message[sizeof(int)];
	int gameLoc;//count to find the game

	int failed;
	char* userName; //the user name of the current player
	char* otherUser; //other players username
	int wins; //number of wins of the other player
	int losses; //number of losses of the other player
	int ties;

	Player *temp;
	TicTacToe *tempGame;



	bool foundPlayer = false;

	//lock the online sessions
	pthread_mutex_lock(&onlineSessionMTX);


	//find the userName
	for(int i = 0; i < onlineSessions.size(); i++){
		//if the socks are equal get the user name
		if(onlineSessions[i].socketNum == ((Carrier*)info)->socket){
			foundPlayer = true; //got the username
			userName = onlineSessions[i].username; //copy username over
			break;
		}
	}

	//unlock the online session!!
	pthread_mutex_unlock(&onlineSessionMTX);


	if(foundPlayer){
		foundPlayer = false; //set back to not found
		//lock the player vector!
		pthread_mutex_lock(&playersMTX);

		//loop through and get the player
		for(int i = 0; i < players.size(); i++){
			if(strcmp(players[i]->getName(), userName) == 0){
				foundPlayer = true; //we found the guy
				
				temp = players[i];				
				break;
			}
		}

		//unlock the player vector!!
		pthread_mutex_unlock(&playersMTX);

	}
	

	//lock the games available
	pthread_mutex_lock(&gamesAvailMTX);

	bool foundGame = false;
  //found the player, then get the information for the write back 
	if(foundPlayer){
    //search for the game id
		for(gameLoc = 0; gameLoc < gamesAvail.size(); gameLoc++){

			if(gamesAvail[gameLoc]->getGameID() == gameID){
				foundGame = true;//we got  it!
				break;
			}
		}

    //found the game now lets add the player
		if(foundGame){
			gamesAvail[gameLoc]->addPlayer(temp); //add the player
      gamesAvail[gameLoc]->setSocket(G_PLAYER2, ((Carrier*)info)->socket); //set the socket number for Player 2
			otherUser = gamesAvail[gameLoc]->getPlayer(G_PLAYER1); //other username

			wins = gamesAvail[gameLoc]->getWins(G_PLAYER1); //get the wins of the other user
			losses = gamesAvail[gameLoc]->getLosses(G_PLAYER1); //get the losses of the other user
      ties = gamesAvail[gameLoc]->getTies(G_PLAYER1); //get the ties of the other use

			tempGame = gamesAvail[gameLoc]; //copy the game over to move into game in session vector
		  gamesAvail.erase(gamesAvail.begin() + gameLoc); //remove pointer to the game from games abail

		}else{
      //we failed to add you to the game, write back -1 
			failed = -1;
			for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&failed, sizeof(failed)) < sizeof(failed);){
				if(written == -1){
		
					cout << "invalid write login " << endl;


					//unlock the player vector!!
          pthread_mutex_unlock(&gamesAvailMTX);



					delete[] ((Carrier*)info)->message;
					delete[] ((Carrier*)info);

					pthread_exit(0);
				}
			}

      pthread_mutex_unlock(&gamesAvailMTX);
			pthread_exit(0);
	}
	//unlocking gamesAvail Vector
		pthread_mutex_unlock(&gamesAvailMTX);
		pthread_mutex_lock(&gamesInsessionMTX);

 		//lock the games in session


		gamesInsession.push_back(tempGame); //add the game into the game in session vector

	//unlocking gamesInSession Vector
	pthread_mutex_unlock(&gamesInsessionMTX);


	if(foundGame && foundPlayer){

		int sizeOfWriteBack = strlen(otherUser)+1 + (sizeof(int)*4);//the size of message to write back
		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&sizeOfWriteBack, sizeof(sizeOfWriteBack)) < sizeof(sizeOfWriteBack);){
			if(written == -1){
		
				cout << "invalid write login joingame " << endl;
				//unlock the player vector!!


				delete[] ((Carrier*)info)->message;
				delete[] ((Carrier*)info);

				pthread_exit(0);
			}
		}

		char writeBack[sizeOfWriteBack];
		writeBack[0] = strlen(otherUser)+1;

		memcpy(writeBack + sizeof(int), otherUser, strlen(otherUser)+1);//copy the user name over

    for(int q = 0; q < strlen(otherUser)+1; q++){
      cout << writeBack[4 + q];
    }
    cout << endl;
		writeBack[sizeof(int) + strlen(otherUser)+1] = wins; //set the wins to writeback

		writeBack[(sizeof(int)*2) + strlen(otherUser)+1 ] = losses; //set the losses to writeback
		writeBack[(sizeof(int)*3) + strlen(otherUser)+1] = ties; //set the ties to the writeback

		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&writeBack, sizeof(writeBack)) < sizeof(writeBack);){
			if(written == -1){

				cout << "invalid write joingame " << endl;
				//unlock the player vector!!


				delete[] ((Carrier*)info)->message;
				delete[] ((Carrier*)info);

				pthread_exit(0);
			}
		}
	}
	else{
    //failed to add the player to the game
		failed = -1;
		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&failed, sizeof(failed)) < sizeof(failed);){
			if(written == -1){

				//unlock the player vector!!

				delete[] ((Carrier*)info)->message;
				delete[] ((Carrier*)info);

				pthread_exit(0);
			}
		}
	 }
	}
	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);
	pthread_exit(0);
}

//thread to delete the player 
//will not delete player if multiple login of the same user
//deletePlayer if failed -1
void *deletePlayer(void* info){

	char* userName;
	int numOfUsersLoggedOn = 0;
	int writeback;
 	
  //lock the online session vector!
	pthread_mutex_lock(&onlineSessionMTX);
  //search for the player in online sessons
	for(int i = 0; i < onlineSessions.size(); i++){
		if(onlineSessions[i].socketNum == ((Carrier*)info)->socket){
			userName = onlineSessions[i].username; //found the username
		}
	}

	for(int i = 0; i < onlineSessions.size(); i++){
		if(strcmp(onlineSessions[i].username,userName) == 0){
			numOfUsersLoggedOn++; //count how many clients are logged on 
		}
	}

	pthread_mutex_unlock(&onlineSessionMTX);

	if(numOfUsersLoggedOn > 1){
    //failed to delete if only one client 
		writeback = -1;
	}else{
		pthread_mutex_lock(&playersMTX);

		for(int i = 0; i < players.size(); i++){
			if(strcmp(players[i]->getName(),userName)==0){
				players.erase(players.begin()+i);
			    writeback = 1;
			}

		}
		writeback = 1;
		for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&writeback, sizeof(writeback)) < sizeof(writeback);){
		  if(written == -1){

		    
		  }
		}
		  pthread_mutex_unlock(&gamesAvailMTX);
		  pthread_mutex_unlock(&playersMTX);

		logout(info);
		cerr << "socket: " << ((Carrier*)info)->socket << endl;
	
	}
	
	for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&writeback, sizeof(writeback)) < sizeof(writeback);){
	    if(written == -1){
	      
	      //unlock the player vector!!

	      pthread_mutex_unlock(&gamesAvailMTX);

	      
	      delete[] ((Carrier*)info)->message;
	      delete[] ((Carrier*)info);
	      
	      pthread_exit(0);
	    }
	  }
  
  pthread_mutex_unlock(&playersMTX);
	pthread_mutex_unlock(&gamesAvailMTX);

	pthread_exit(0);
}

//logout 
//client writes message (int logot module)
void *logout(void* info){

  char* userName;


	//lock the online session vector!
	pthread_mutex_lock(&onlineSessionMTX);

  //find the player in the online sesson
	for(int i = 0; i < onlineSessions.size(); i++){
		if(onlineSessions[i].socketNum == ((Carrier*)info)->socket){
      userName = onlineSessions[i].username;
			onlineSessions.erase(onlineSessions.begin()+i);
		}
	}


	//unlock the player vector!!
	pthread_mutex_unlock(&onlineSessionMTX);
 

  pthread_mutex_lock(&gamesAvailMTX);
  //see if the user is in a game avalable
  for(int i = 0; i < gamesAvail.size(); i++){
    if(strcmp(gamesAvail[i]->getPlayer(G_PLAYER1),userName) == 0){
      gamesAvail.erase(gamesAvail.begin()+i); //remove this game 
    }
  }

  pthread_mutex_unlock(&gamesAvailMTX);

  pthread_mutex_lock(&gamesInsessionMTX);
  
  //see if the player is in a game that is insession
  //removed the player, the other player is notified via game status
  for(int i = 0; i < gamesInsession.size(); i++){
  
    if(gamesInsession[i]->getPlayer(G_PLAYER1)!=NULL){
      if(strcmp(gamesInsession[i]->getPlayer(G_PLAYER1),userName) == 0 && ((Carrier*)info)->socket == gamesInsession[i]->getSocket(G_PLAYER1)){

      gamesInsession[i]->removePlayer(G_PLAYER1);
      }
    }
    if(gamesInsession[i]->getPlayer(G_PLAYER2)!=NULL){
      if(strcmp(gamesInsession[i]->getPlayer(G_PLAYER2),userName) == 0 && ((Carrier*)info)->socket == gamesInsession[i]->getSocket(G_PLAYER2)){

      gamesInsession[i]->removePlayer(G_PLAYER2);
      }
    }
    
  }

  pthread_mutex_unlock(&gamesInsessionMTX);


	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);

	pthread_exit(0);
}

//thread for the host module, client polls this frequently to get the status of the game they created(if a user
// has joined the game)
//Client data format: (<# for host module>, int gameId,username)
//erver: checks if this player is really part of gameID, then check if p2 is allocated, if so return p2 details:
//Server data format: (int gameIdNum, string username of other player, int wins, int losses)
//If no second player YET, return to user: (-1)
void *hostModulePoll(void* info){
	using namespace std;
	

	int gameID = ((Carrier*)info)->message[sizeof(int)];
	
	int foundGame = -1;
	int gotPlayer2 = -1;


	int userNameLength = ((Carrier*)info)->message[sizeof(int)*2];
	int userNamePos = sizeof(int)*3;
	char *userName = new char[userNameLength];


 
	//gets username and password from data buffer
	memcpy(userName, ((Carrier*)info)->message+userNamePos, userNameLength);
 
	//lock the games available vector!
	pthread_mutex_lock(&gamesInsessionMTX);

	int i;
	for(i = 0; i < gamesInsession.size(); i++){
		//found the game!
		if(gamesInsession[i]->getGameID() == gameID){
      //check to see if this user is the user in the game
			if(strcmp(gamesInsession[i]->getPlayer(G_PLAYER1),userName) == 0){
 
				foundGame = 1;
        //check to see if the other player exists
				if(gamesInsession[i]->getPlayer(G_PLAYER2) != NULL){
     
			
					gotPlayer2 = 1;
				}
			}
      break;
		}
	}

	if(foundGame == 1 && gotPlayer2 == 1){
		int sizeOfUserName = strlen(gamesInsession[i]->getPlayer(G_PLAYER2))+1; //get the username of the player


		//need to write back int lenth of other person username, int username, int wins, int loses
		int sizeOfWriteBack = (sizeof(int) * 4) + sizeOfUserName; //calculate size of message
				
		for(int written = 0; written+= write(((Carrier*)info)->socket, (char *)&sizeOfWriteBack, sizeof(sizeOfWriteBack))<sizeof(sizeOfWriteBack);){
			if(written = -1){
				//exit
				//debug
				cout << "invalid write, size of writeBack" << endl;
				pthread_mutex_unlock(&gamesInsessionMTX);
				pthread_exit(0);


			}
		} 
		//write back the size of the message
		cout << "wrote " << sizeOfWriteBack << " size" << endl;
		char writeBack[sizeOfWriteBack]; //message back
			
	
		writeBack[0] = sizeOfUserName; //store length of username


		memcpy(writeBack + sizeof(int), gamesInsession[i]->getPlayer(G_PLAYER2), sizeOfUserName); //store user name for msg to client

   
		int temp = (sizeof(int)) + sizeOfUserName; //pos of wins int
		writeBack[temp] = gamesInsession[i]->getWins(G_PLAYER2); //store wins for writeback
    
		temp+=sizeof(int); //pos of losses int
		writeBack[temp] = gamesInsession[i]->getLosses(G_PLAYER2);//store losses for message to client

    temp+=sizeof(int); //pos of losses int
    writeBack[temp] = gamesInsession[i]->getTies(G_PLAYER2);//store losses for message to client

	
  	//write message back to the server
		for(int written = 0; written+=write(((Carrier*)info)->socket, (char *)writeBack, sizeof(writeBack)) < sizeof(writeBack);){
			if(written = -1){
				cout << "invalid write, writeing back the message" << endl;
				pthread_mutex_unlock(&gamesInsessionMTX);					
				pthread_exit(0); //no games avail, exit thread
			}
		} 
	
	}else{
    //there is no second player yet!
	  int failed = -1;
	  for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&failed, sizeof(failed)) < sizeof(failed);){
	    if(written == -1){
	    
	      cout << "invalid write login join game" << endl;
	      //unlock the game vector!!

	      pthread_mutex_unlock(&gamesInsessionMTX);

	      
	      delete[] ((Carrier*)info)->message;
	      delete[] ((Carrier*)info);
	      
	      pthread_exit(0);
	    }
	  }

    pthread_mutex_unlock(&gamesInsessionMTX);
    delete[] ((Carrier*)info)->message;
    delete[] ((Carrier*)info);
        
    pthread_exit(0);
	}



	// the online sessions vector!!

  pthread_mutex_unlock(&gamesInsessionMTX);

	delete[] ((Carrier*)info)->message;
	delete[] ((Carrier*)info);
	pthread_exit(0);
}


//poll for when players a in the game, to get the status of the board 
//Client sends: (<# for check if other player made a move module>,gameID)
//Server: checks whos turn it is, if it isn’t the calling players turn then return -1 to client 
//your turn: if -1, then not your turn, if 1 your turn, -2 other player Dced
//gameArray: int array[9], 0 = playable, 1 = player 1, 2 = player 2
//gameover: not over = -1, over - you won = 0, over other player won = 1
//int winType:   0 = horizontal top line, 1 = horizontal middle line, 2 = horizontal bottom line,
 //3 = verticle left line, 4 = vertical middle line, 5 = vertical right line, 6 = diagonal line from left to right,
 // 7 = diagonal line from top right to bottom left

void *gameStatusModule(void * info){
  using namespace std;
  
  int gameID = ((Carrier*)info)->message[sizeof(int)];
  
  int foundGame = -1;
  int gotPlayer2 = -1;
  bool foundPlayer;
  char* userName;

  //lock 
  //lock the online sessions
  pthread_mutex_lock(&onlineSessionMTX);


  //find the userName
  for(int k = 0; k < onlineSessions.size(); k++){
    //if the socks are equal get the user name
    if(onlineSessions[k].socketNum == ((Carrier*)info)->socket){
      //we  got the player
      foundPlayer = true;
      userName = onlineSessions[k].username;
      break;
    }
  }

  //unlock the online session!!
  pthread_mutex_unlock(&onlineSessionMTX);

  //lock the games available vector!
  pthread_mutex_lock(&gamesInsessionMTX);
  int gameLoc;
  //find the game via gameID
  for(gameLoc = 0; gameLoc < gamesInsession.size(); gameLoc++){
  
    if(gamesInsession[gameLoc]->getGameID() == gameID){
      
      if(int curplaynum = gamesInsession[gameLoc]->getPlayerNum(userName) !=-1){
        //found the game, and the cur player possition
        foundGame = 1;
        if(gamesInsession[gameLoc]->getPlayerNum(userName)== G_PLAYER1){
        	if(gamesInsession[gameLoc]->getPlayer(G_PLAYER2) != NULL){
              //the other player has not left
          		gotPlayer2 = 1;
        	}
        }else if(gamesInsession[gameLoc]->getPlayerNum(userName)== G_PLAYER2){
        	if(gamesInsession[gameLoc]->getPlayer(G_PLAYER1) != NULL){
            //the other player has not left
        		gotPlayer2 = 1;
        	}
        }
        
      }
      break;
    }
  }

  int sizeOfWriteBack = sizeof(int)*12; //set the writeback size

  char *writeBack = new char[sizeOfWriteBack];

  //if we got the game, and the other player is still there, write back
  if(foundPlayer && foundGame == 1 && gotPlayer2 == 1){
  	 
     //check to see if it is there turn
    if(gamesInsession[gameLoc]->isTurn(userName)){
    	
      writeBack[0] = 1;
    }else{
      writeBack[0] = -1;
    }


    int* board = gamesInsession[gameLoc]->getBoard(); //get the most up to date board
 
    memcpy(writeBack+sizeof(int),board, (sizeof(board)+1)*sizeof(int)); //copy the board over

    //check to see if the game is over aka, a win, loss or tie
    if(gamesInsession[gameLoc]->checkWin() != -1){

    	int winner = gamesInsession[gameLoc]->getWinStatus(); //get the winner of the game, 1 player1, 2 p2, 3tie
    	int curPlayer = gamesInsession[gameLoc]->getPlayerNum(userName);//get the current player's possition (1 or 2)

      //check cases for the write back of the game status
      if(winner == 3){
        writeBack[sizeof(int)*10] = 2; //tie
      }
    	else if(winner == curPlayer){
    		writeBack[sizeof(int)*10] = 0; //cur player has won
    	}else{ 
    		writeBack[sizeof(int)*10] = 1;//other player has won
    	}
    	
     }else{
    	writeBack[sizeof(int)*10] = -1;//game still going!
    }

    writeBack[sizeof(int)*11] = gamesInsession[gameLoc]->checkWin(); //retirns the type of win
 
  
    for(int written = 0; written+=write(((Carrier*)info)->socket, (char *)writeBack, sizeOfWriteBack) < sizeOfWriteBack;){
      if(written = -1){
        
	       delete[] writeBack;
        pthread_mutex_unlock(&gamesInsessionMTX);         
        pthread_exit(0); //no games avail, exit thread
      }
    } 
  
  }else{
    //the other player has left! or game corrupt 
    writeBack[0] = -2;
    gamesInsession.erase(gamesInsession.begin()+gameLoc);
    for(int written = 0; written += write(((Carrier*)info)->socket, (char *)writeBack, sizeOfWriteBack) < sizeOfWriteBack;){
      if(written == -1){
        //unlock the player vector!!
        pthread_mutex_unlock(&gamesInsessionMTX);

        delete[] writeBack;
        delete[] ((Carrier*)info)->message;
        delete[] ((Carrier*)info);
        
        pthread_exit(0);
      }
    }
  }
  
  if(gamesInsession[gameLoc]->checkWin()!=-1){
    if(gamesInsession[gameLoc]->getFlag()){
      gamesInsession.erase(gamesInsession.begin()+gameLoc); //if win and the flag has been raised delete the game


    }else{
      pthread_mutex_lock(&playersMTX);

      std::sort(players.begin(),players.end()); //update the players for scoreboard
      pthread_mutex_unlock(&playersMTX);
      gamesInsession[gameLoc]->setFlag(); //raise the flag if not already 
    }
  }
  //unlock the online sessions vector!!
  pthread_mutex_unlock(&gamesInsessionMTX);

  delete[] ((Carrier*)info)->message;
  delete[] ((Carrier*)info);
  pthread_exit(0);
}

//thread for making a move make move
//If making a move:
//Client sends (<# for to make move module>, int spot to mark, int gameid)
//Server: checks if user is part of game (as always u should have had sanity checks), is his turn, 
//checks if that spot is used or not, and marks it if okay) and marks game as other players turn
//Server send: (int status) 1=  success; -1 = aint yo turn, 3 = wtf square in use 
//(even though the client side should obviously have code to check for this already…)
void *makeMove(void * info){

  using namespace std;
  
  int move = ((Carrier*)info)->message[sizeof(int)];
  int gameID = ((Carrier*)info)->message[sizeof(int)*2];

  int foundGame = -1;
  bool foundPlayer = false;
  char* userName;

  int writeBack; 

  //lock 
  //lock the online sessions
  pthread_mutex_lock(&onlineSessionMTX);

  //find the userName
  for(int i = 0; i < onlineSessions.size(); i++){
    //if the socks are equal get the user name
    if(onlineSessions[i].socketNum == ((Carrier*)info)->socket){
      foundPlayer = true;
      userName = onlineSessions[i].username;
      break;
    }
  }

  //unlock the online session!!
  pthread_mutex_unlock(&onlineSessionMTX);

  //lock the games insession vector!
  pthread_mutex_lock(&gamesInsessionMTX);


  int gameLoc;
  for(gameLoc = 0; gameLoc < gamesInsession.size(); gameLoc++){
  	if(gamesInsession[gameLoc]->getGameID() == gameID){
      //found the game lovation 
  		break;
  	}  	
  }
  //check to see if it really is this players turn, although there are checks
  //on the client
  if(gamesInsession[gameLoc]->isTurn(userName)){
    
  	writeBack = 1; //took the turn 
  	if(!gamesInsession[gameLoc]->takeTurn(move)){

  		writeBack = 3;//invalid move, although there are checks on the clients
  	}
  }else{
  	writeBack = -1; //could not make the move, not your turn! though again client checks
  }

  //unlock the games insession vector!
  pthread_mutex_unlock(&gamesInsessionMTX);
  for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&writeBack, sizeof(writeBack)) < sizeof(writeBack);){
			if(written == -1){
				
		}
	}
  
  delete[] ((Carrier*)info)->message;
  delete[] ((Carrier*)info);
  pthread_exit(0);
}

//message from client (int module)
//only quite game module from the client
//no message back from the server
//each socket can only be in 1 game at a time, all though the user can be in multple games
//via multiple logins. will only remove the player from the game that this socket is in
void *quitGame(void * info){
  using namespace std;

  char* userName;
  bool foundUser = false;
  
  pthread_mutex_lock(&gamesInsessionMTX);
  //search for the user in the games in sessions 

  for(int i = 0; i < gamesInsession.size(); i++){
    if(gamesInsession[i]->getSocket(G_PLAYER1) == ((Carrier*)info)->socket){
      gamesInsession[i]->removePlayer(G_PLAYER1); //remove the player
      pthread_mutex_unlock(&gamesInsessionMTX); 
      pthread_exit(0);
    }
    else if(gamesInsession[i]->getSocket(G_PLAYER2) == ((Carrier*)info)->socket){
      gamesInsession[i]->removePlayer(G_PLAYER2); //remove the player
      pthread_mutex_unlock(&gamesInsessionMTX);
      pthread_exit(0);
    }
  }
  pthread_mutex_unlock(&gamesInsessionMTX);

  //search for the player in games available 
  pthread_mutex_lock(&gamesAvailMTX);
  for(int i = 0; i < gamesAvail.size(); i++){
    if(gamesAvail[i]->getSocket(G_PLAYER1) == ((Carrier*)info)->socket){
      gamesAvail.erase(gamesAvail.begin() + i);//remove the player
      break;
    }
    else if(gamesAvail[i]->getSocket(G_PLAYER2) == ((Carrier*)info)->socket){
      gamesAvail.erase(gamesAvail.begin() + i); //remove the player
      break;
    }
  }
  pthread_mutex_unlock(&gamesAvailMTX);
  pthread_exit(0);
}


//thread to recieve a message from the client to send to the other player in the game
//client sends (int module, message)
//no response from the server (send and forget!)
//message size is always a constant 201 bytes (200 chars allowed by the user + null termenation char)
void *sendMessage(void* info){

  Message messageInfo; //the message object
  char *temp = new char[SIZEOFMESSAGE];
  memset(temp, 0, SIZEOFMESSAGE);
  int destSock;
  messageInfo.message=temp;
  memcpy(temp, ((Carrier*)info)->message + sizeof(int), SIZEOFMESSAGE); //copy the message over to the struct


  
  
  pthread_mutex_lock(&gamesInsessionMTX);
  //search for the game
  for(int i = 0; i < gamesInsession.size(); i++){
    if(gamesInsession[i]->getSocket(G_PLAYER1) == ((Carrier*)info)->socket){
        if(gamesInsession[i]->getSocket(G_PLAYER2)!=-1){
          destSock = gamesInsession[i]->getSocket(G_PLAYER2); //get the other players socket
        }
        break;
    }
    if(gamesInsession[i]->getSocket(G_PLAYER2) != -1){
      if(gamesInsession[i]->getSocket(G_PLAYER1)!=-1){
          destSock = gamesInsession[i]->getSocket(G_PLAYER1); //get the other players socket
        }
        break;
    }
  }
  messageInfo.sock = destSock;
  pthread_mutex_unlock(&gamesInsessionMTX);


  pthread_mutex_lock(&messageMTX);
  messageVect.push_back(messageInfo); //put the message in the vector to store
  pthread_mutex_unlock(&messageMTX);

}

//thread for the client to poll for new messages while in a game
//client sends (int module)
//server sends (message)
//message size is always a constant 201 bytes (200 chars allowed by the user + null termenation char)
void *pollMessage(void* info){
  char* messageToSend[SIZEOFMESSAGE];
  bool messageFound = false;
  int writeIsMessage;

  pthread_mutex_lock(&messageMTX);
  //check the message vector too see if there are new messages for this socket
  for(int i = 0; i < messageVect.size(); i++){
    if(messageVect[i].sock == ((Carrier*)info)->socket){
      memcpy(messageToSend,messageVect[i].message, SIZEOFMESSAGE); //copy new message over to send
      messageFound = true; //found the message
      messageVect.erase(messageVect.begin() +i); //erase this message object
      break;
    }
  }
  pthread_mutex_unlock(&messageMTX);

    if(messageFound){
    //found message so send it to the client
    writeIsMessage = 1;
    for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&writeIsMessage, sizeof(writeIsMessage)) < sizeof(int);){
      if(written == -1){
        cout << "invalid write pollMessage" << endl;
      }
    }

    for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&messageToSend, SIZEOFMESSAGE) < SIZEOFMESSAGE;){
      if(written == -1){
        cout << "invalid write make move" << endl;
      }
    }
  }
  else{
    //no new messages!
    writeIsMessage = -1;
    for(int written = 0; written += write(((Carrier*)info)->socket, (char *)&writeIsMessage, sizeof(writeIsMessage)) < sizeof(int);){
      if(written == -1){
  
        cout << "invalid write pollMessage" << endl;
  
      }
    }
  }
  pthread_exit(0);
}
