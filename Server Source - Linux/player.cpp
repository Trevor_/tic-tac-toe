#include "player.h"
#include <string.h>
#include <iostream>

//default constructor
Player::Player(){
	wins = 0;
	losses = 0;
	ties = 0;
}
//-------------------------------------------------------------------
//construct to initilize name nad pass
Player::Player(char* name, char* pass){
	userName = name;
	password = pass;
	wins = 0;
	losses = 0;
	ties = 0;
}

//-------------------------------------------------------------------
//copy constructor
Player::Player(const Player &right){
	userName = new char(strlen(right.userName));
	userName = right.userName;
	password = new char(strlen(right.password));
	password = right.password;
	wins = right.wins;
	losses = right.losses;
	ties = right.ties;

}
//-------------------------------------------------------------------
void Player::addWin(){
	wins++;
}

//-------------------------------------------------------------------
void Player::addLoss(){
	losses++;
}
//-------------------------------------------------------------------
char* Player::getName(){
	return userName;
}
//-------------------------------------------------------------------
char* Player::getPass(){
	return password;
}
//-------------------------------------------------------------------
bool Player::operator==(Player left){
	return this->userName == left.userName;
}

//-------------------------------------------------------------------
bool Player::validPass(Player left){
	return this->password == left.password;
}
//-------------------------------------------------------------------
bool Player::operator!=(Player left){
	return this->userName != left.userName;
}
//-------------------------------------------------------------------
int Player::getWins(){
	return wins;
}
//-------------------------------------------------------------------
int Player::getLosses(){
	return losses;
}

//-------------------------------------------------------------------
bool Player::operator<(const Player & p2){
	if (wins != p2.wins)
	{
		return wins < p2.wins;
	} 
	if (wins == p2.wins && losses == p2.losses) 
		{
			return ties < p2.ties;
		}

	return losses > p2.losses;
}

//-------------------------------------------------------------------
bool Player::operator>(const Player & p2){
	if (wins != p2.wins) return wins > p2.wins;
	if (wins == p2.wins && ties != p2.ties) return ties > p2.ties;
	if(wins == p2.wins && ties == p2.ties) return losses < p2.losses;
	return true;
}
//-------------------------------------------------------------------
int Player::getTies(){
	return ties; 
}
//-------------------------------------------------------------------
void Player::addTie(){
	ties++;
}
