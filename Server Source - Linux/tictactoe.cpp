#include "tictactoe.h"

TicTacToe::TicTacToe(){
	turn = 1;
	winStatus =0;
	player1 = NULL;
	player2 = NULL;
	addedWins = false;
	numberOfTurns = 0;
	socketPlayer1 = -1;
	socketPlayer2= -1;
	board = new int[9];
	for(int i = 0; i < 9 ; i++)
	  board[i] =0;

}
//constructor
TicTacToe::TicTacToe(Player* one, Player* two){
	this->player1 = one;
	this->player2 = two;
	turn = 1;
	winStatus = 0;
	addedWins = false;
	numberOfTurns = 0;
	socketPlayer1 = -1;
	socketPlayer2= -1;
    board = new int[9];
	for(int i = 0; i < 9 ; i++)
	  board[i] = 0;
}

//copy constructor
TicTacToe::TicTacToe(const TicTacToe &right){
	this->player1 = new Player();
	player1 = right.player1;
	flag = right.flag;
	player2 = new Player();
	player2 = right.player2;
	addedWins = right.addedWins;
	numberOfTurns = right.numberOfTurns;
	socketPlayer1 = -1;
	socketPlayer2= -1;
    board = new int[9];

	turn = right.turn;
	winStatus = right.winStatus;

	for(int i = 0; i < 9 ; i++) {
	  board[i] = 0;
	}
	for(int i = 0; i < 9; i++){
		board[i] = right.board[i];
	}
}

//---------------------------------------------------------------
//tac a turn in the game
bool TicTacToe::takeTurn(int playerTurn){
	if(playerTurn < 0 || playerTurn > 8)
		return false;
	if(board[playerTurn]!=0){
		return false;
	}
	if(numberOfTurns>9){
		return false;
	}
	if(checkWin()!=-1){
		return false;
	}
	if(turn == 1){
		if(board[playerTurn]==0){
			board[playerTurn] = 1;
			turn = 2;
			checkWin();
			numberOfTurns++;
			return true;
		}
	} 
	else if(turn == 2){
		if(board[playerTurn]==0){
			board[playerTurn] = 2;
			turn = 1;
			checkWin();
            numberOfTurns++;
			return true;
		}
	}
	return false;
}

//---------------------------------------------------------------
//check for a tie
bool TicTacToe::isTie(){
	return checkWin() == -2;
}
//---------------------------------------------------------------
//check for a win
int TicTacToe::checkWin(){
	if(horizontalWin() != -1 || verticalWin() != -1 || diagonalWin() !=-1){
		if(!addedWins){
			addedWins = true;
			if(winStatus == 1){
				player1->addWin();
				player2->addLoss();
			}
			else if(winStatus == 2){
				player1->addLoss();
				player2->addWin();
			}
		}
	}
	if(horizontalWin() !=-1){
		return horizontalWin();
	}
	else if(verticalWin() !=-1){
		return verticalWin();
	}
	else if(diagonalWin() != -1){
		return diagonalWin();
	}

	if(numberOfTurns >= 9){
	  if (!addedWins) {
	    addedWins = true;
		winStatus = 3;
		player1->addTie();
		player2->addTie();
		return -2;
	  }
	}
	return -1;
}
//---------------------------------------------------------------
//horizontal win
int TicTacToe::horizontalWin(){
	if(board[0] == board[1] && board[1] == board[2]){
			if(board[0]==1){
				winStatus = 1;
				return 0;
			}
			else if(board[0] == 2){
				winStatus = 2;
				return 0;
			}
	}
	else if(board[3] == board[4] && board[4] == board[5]){
			if(board[3]==1){
				
				winStatus = 1;
				return 1;
			}
			else if(board[3] ==2){
				
				winStatus = 2;
				return 1;
			}
	}
	else if(board[6] == board[7] && board[7] == board[8]){
			if(board[6]==1){
				
				winStatus = 1;
				return 2;
			}
			else if(board[6]==2){
			
				winStatus = 2;
				return 2;
			}
	}
	return -1;	 
}
//---------------------------------------------------------------
//vertical win
int TicTacToe::verticalWin(){
	if(board[0] == board[3] && board[3] == board[6]){
			if(board[0]==1){
			
				winStatus = 1;
				return 3;
			}
			else if(board[0] == 2){
			
				winStatus = 2;
				return 3;
			}
	}
	else if(board[1] == board[4] && board[4] == board[7]){
			if(board[1]==1){
				
				winStatus = 1;
				return 4;
			}
			else if(board[1] == 2){
				
				winStatus = 2;
				return 4;
			}
	}
	else if(board[2] == board[5] && board[5] == board[8]){
			if(board[2]==1){
			
				winStatus = 1;
				return 5;
			}
			else if(board[2] ==2){
				
				winStatus = 2;
				return 5;
			}
	}
	return -1;	 
}
//---------------------------------------------------------------
//diagonalWin
int TicTacToe::diagonalWin(){
		if(board[0] == board[4] && board[4] == board[8]!=0){
			if(board[4]==1){
				
				winStatus = 1;
				return 6;
			}
			else if(board[2] == 2){
	
				winStatus = 2;
				return 6;
			}
		}
		else if(board[2] == board[4] && board[4] == board[6]){
			if(board[4]==1){
				
				winStatus = 1;
				return 7;
			}
			else if(board[4]==2){
			
				winStatus = 2;
				return 7;
			}
		}
	return -1;	 
}
//---------------------------------------------------------------
void TicTacToe::reset(){
	for(int i = 0; i < 9; i++){
		board[i] = 0;
	}
	turn = 1;
	flag = false;
	winStatus = -1;
	addedWins = false;
	numberOfTurns = 0;

}

//---------------------------------------------------------------
bool TicTacToe::addPlayer(Player* adding){
	if(player1 == NULL){
		player1 = adding;
	}else if (player2 == NULL){
		player2 = adding;
	}else{
		return false;
	}
	return true;
}

//---------------------------------------------------------------
bool TicTacToe::gameAvailable(){
	return player1 == NULL || player2 == NULL;
}
//-------------------------------------------------------------------
bool TicTacToe::empty(){
	return player1 == NULL && player2 == NULL;
}
//-------------------------------------------------------------------
char* TicTacToe::getPlayer(int val){
	if(val == 1 && player1!=NULL){
		return player1->getName();
	}
	if(val == 2 && player2!= NULL){
		return player2->getName();
	}
	return NULL;
}
//-------------------------------------------------------------------
int TicTacToe::getWins(int player){
	if(player == -1 && player2 == NULL){
		return player1->getWins();
	}
	else if(player == -1 && player1 == NULL){
		return player2->getWins();
	}else if(player == 1){
		return player1->getWins();
	}else if(player == 2){
		return player2->getWins();
	}
}
//-------------------------------------------------------------------
int TicTacToe::getLosses(int player){
	if(player == -1 && player2 == NULL){
		return player1->getLosses();
	}
	else if(player == -1 && player1 == NULL){
		return player2->getLosses();
	}else if(player == 1){
		return player1->getLosses();
	}else if(player == 2){
		return player2->getLosses();
	}
}
//-------------------------------------------------------------------
int TicTacToe::getTies(int player){
  if(player == -1 && player2 == NULL){
    return player1->getTies();
  }
  else if(player == -1 && player1 == NULL){
    return player2->getTies();
  }else if(player == 1){
    return player1->getTies();
  }else if(player == 2){
    return player2->getTies();
  }
}

//-------------------------------------------------------------------
int TicTacToe::getGameID(){
	return this->gameID;
}

//-------------------------------------------------------------------
void TicTacToe::setGameID(int val){
	this->gameID = val;
}


//-------------------------------------------------------------------
bool TicTacToe::checkPlayer(char * userName, int player){
	if(player == 1){
		return strcmp(player1->getName(),userName) == 0;
	}
	else if(player == 2){
		return strcmp(player2->getName(),userName) == 0;
	}
	return false;
}

//-------------------------------------------------------------------
int TicTacToe::getPlayerNum(char * userName){
	if(player1!=NULL){
		if(strcmp(player1->getName(), userName) == 0){
		return 1;
	 }
	}
	if(player2!=NULL){
		if(strcmp(player2->getName(), userName) == 0){
			return 2;
		}		
	}

	return -1; 
}


//-------------------------------------------------------------------
void TicTacToe::removePlayer(int player){
	if(player == 1){
		socketPlayer1 = -1;
		player1 = NULL;
	}else if(player == 2){
		player2 = NULL;
		socketPlayer2 = -1;
	}
}
//-------------------------------------------------------------------
int TicTacToe::getWinStatus(){
	return winStatus;
}
//-------------------------------------------------------------------
bool TicTacToe::isTurn(char* userName){

	if(strcmp(player1->getName(),userName) == 0 && turn == 1){
	
		return true;
	}
	if(strcmp(player2->getName(),userName) == 0 && turn == 2){

		return true;
	}
	return false;
}
//-------------------------------------------------------------------
int* TicTacToe::getBoard(){
	return board;
}
//-------------------------------------------------------------------
void TicTacToe::setSocket(int player, int socket){
	if(player == 1){
		socketPlayer1 = socket;
	}
	if(player == 2){
		socketPlayer2 = socket;
	}
}
//-------------------------------------------------------------------
int TicTacToe::getSocket(int player){
	if(player == 1){
		return socketPlayer1;
	}
	if(player == 2){
		return socketPlayer2;
	}
}
//-------------------------------------------------------------------
void TicTacToe::setFlag(){
	flag = true;
}
bool TicTacToe::getFlag(){
	return flag;
}
