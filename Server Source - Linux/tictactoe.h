
#ifndef TICTACTOE
#define TICTACTOE

#include "player.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

class TicTacToe{

public:
	TicTacToe();
	TicTacToe(const TicTacToe&);
	TicTacToe(Player*,Player*);
	void reset();
	bool takeTurn(int);


	int checkWin();
	int verticalWin();
	int horizontalWin();
	int diagonalWin();
	bool isTie();
	bool addPlayer(Player*);
	bool gameAvailable();

	bool checkPlayer(char*,int);

	char* getPlayer(int = -1);
	int getWins(int = -1);
	int getLosses(int = -1);
	int getTies(int = -1);
	bool empty();

	int getGameID();
	void setGameID(int);

	int getPlayerNum(char*);
	void removePlayer(int);

	void setSocket(int,int);
	int getSocket(int);

	int getWinStatus();
	bool isTurn(char*);
	void setFlag();
	bool getFlag();

	int *getBoard();


private:

	Player* player2;
    Player* player1;


	//the tic tac to board:
	//012
	//345
	//678
	//0 = no one is in there
	//1= player1 /O
	//2= player2 /X
	int* board;
	
	int turn; //1 = player 1, 2 = player 2

	int numberOfTurns; //counter for the number of turns taken

	int gameID; //id of this game

	bool addedWins; //flag to see if wins has been incremented
	
	int winStatus; //check to see who won: 1 = player1, 2 = player2, 3 = tie

	int socketPlayer1;
	int socketPlayer2;

	bool flag; //flag to see if the other client has recieved data during the game, prior
			   //to deleting this object
};

#endif
