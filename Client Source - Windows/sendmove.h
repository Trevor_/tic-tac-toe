#ifndef SENDMOVE_H
#define SENDMOVE_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class sendMove : public QThread {
    Q_OBJECT
    public:
        void run();
        void setAction(int spotMoved);

        signals:
        void networkFailure();
        void moveUpdated(int status, int move);

    private:
        int spotMoved;

    int pollSocket();

};

#endif // SENDMOVE_H

