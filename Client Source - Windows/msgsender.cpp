#include "msgsender.h"
#include "globaldata.h"

void msgSender::run()
{
    //module to call of server
    int serverFuncID = gData::SEND_MESSAGE_MODULE;
    gData::socketMutex.lock(); //locks socket

    //size of message we are sending server
    int size = sizeof(serverFuncID)+gData::MSG_SIZE;

    //first message is the size of next message
    int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

    if (result == SOCKET_ERROR) { //socket error
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    //sends server (int moduleID, char message)
    //fires and forgets, no response need from server
    char *sendBuf = new char[size]; //send data buffer
    memset(sendBuf, 0, size);
    sendBuf[0] = (int)serverFuncID;
    strcpy((sendBuf+sizeof(int)),msg);
    result = send(gData::ConnectSocket, (char*)sendBuf,size, 0);

    if (result == SOCKET_ERROR) { //socket error
        gData::socketMutex.unlock();
        delete[] sendBuf;
        delete[] msg;
        emit networkFailure();
        return;
    }

    gData::socketMutex.unlock();
    delete[] msg;
    delete[] sendBuf;
}

//sets pointer to message
void msgSender::setMsgPtr(char *message) {
    this->msg = message;
}

//polls socket for data
int msgSender::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
