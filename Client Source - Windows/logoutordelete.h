#ifndef LOGOUTORDELETE_H
#define LOGOUTORDELETE_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class logoutOrDelete : public QThread {
    Q_OBJECT
    public:
        void run();
        void setAction(int action);

        signals:
        void networkFailure();
        void deleted(int status);
        void quitStatus();

    private:
        int action;
        int pollSocket();

};

#endif // LOGOUTORDELETE_H
