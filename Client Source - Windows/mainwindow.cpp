//my modules
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "networkSetup.h"
#include "loginregistermodule.h"
#include "listscores.h"
#include "findgames.h"
#include "creategame.h"
#include "polloncreatedgame.h"
#include "gamepolling.h"
#include "joinexistinggame.h"
#include "logoutordelete.h"
#include "keepalive.h"
#include "sendmove.h"
#include "messagemodule.h"
#include "msgsender.h"

//actual includes
#include <QMessageBox>
#include <QCryptographicHash>
#include <windows.h> // for Sleep
#include <QThread>
#include <QInputDialog>
#include <sstream>
#include <QKeyEvent>


//data initialization occurs in this function
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->userNameField, SIGNAL (returnPressed()) ,SLOT(gotoPassword()));
    connect(ui->passWordField, SIGNAL(returnPressed()),ui->loginButton,SIGNAL(clicked()));
    ui->sendChatBox->installEventFilter(this);
    ui->loggedInFrame->setVisible(false);
    ui->gameFrame->setVisible(false);
    ui->loginScreen->setVisible(true);
    ui->statusBar->showMessage("Connecting to server...");
    ui->selectGameFrame->setVisible(false);
    networkError = false;
    ui->tictactoeFrame->setVisible(false);
    ui->reconectButton->setVisible(false);

    gData::gameBoard = new int[9];
    for (int i = 0; i < 9; i++ ) {
        gData::gameBoard[i] = 0;
    }
    ui->labelPlayersTurn->setVisible(false);
    ui->labelPlayerVS->setVisible(false);

    //configures scoreboard
    ui->scoreTable->setRowCount(gData::numOfTopScores);
    ui->scoreTable->setColumnCount(4);
    ui->scoreTable->setColumnWidth(0,  133);
    ui->scoreTable->setColumnWidth(1,  35);
    ui->scoreTable->setColumnWidth(2,  35);
    ui->scoreTable->setColumnWidth(3,  35);
    ui->scoreTable->setRowHeight(0,  33);
    ui->scoreTable->setRowHeight(1,  33);
    ui->scoreTable->setRowHeight(2,  33);
    ui->scoreTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->scoreTable->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    //pushes data/tables into scoreboard
    for (int i = 0; i < gData::numOfTopScores; i++ ) {
        tableUserName.push_back(new QTableWidgetItem(" "));
        tableWins.push_back( new QTableWidgetItem(" "));
        tableLoses.push_back(new QTableWidgetItem(" "));
        tableTies.push_back(new QTableWidgetItem(" "));
        ui->scoreTable->setItem(i, 0, tableUserName[i]);
        ui->scoreTable->setItem(i, 1, tableWins[i]);
        ui->scoreTable->item(i,1)->setTextAlignment(Qt::AlignCenter);
        ui->scoreTable->setItem(i, 2, tableLoses[i]);
        ui->scoreTable->item(i,2)->setTextAlignment(Qt::AlignCenter);
        ui->scoreTable->setItem(i, 3, tableTies[i]);
        ui->scoreTable->item(i,3)->setTextAlignment(Qt::AlignCenter);
        ui->scoreTable->setRowHidden(i, true);
    }
    //configures available games
    ui->availGamesTable->setColumnWidth(0,  150);
    ui->availGamesTable->setColumnWidth(1,  35);
    ui->availGamesTable->setColumnWidth(2,  35);
    ui->availGamesTable->setColumnWidth(3,  1);
    ui->availGamesTable->setColumnWidth(4,  35);
    ui->availGamesTable->setRowCount(gData::numOfGamesListing);
    ui->availGamesTable->setColumnCount(5);
    ui->availGamesTable->setColumnHidden(3, true);
    ui->availGamesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->availGamesTable->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    //pushes data into available games
    for (int i = 0; i < gData::numOfGamesListing; i++ ) {
        availGameID.push_back(new QTableWidgetItem(""));
        availGameUserName.push_back(new QTableWidgetItem(" "));
        availGameWins.push_back( new QTableWidgetItem(" "));
        availGameLoses.push_back(new QTableWidgetItem(" "));
        availGameTies.push_back(new QTableWidgetItem(" "));
        ui->availGamesTable->setItem(i, 0, availGameUserName[i]);
        ui->availGamesTable->setItem(i, 1, availGameWins[i]);
        ui->availGamesTable->item(i,1)->setTextAlignment(Qt::AlignCenter);
        ui->availGamesTable->setItem(i, 2, availGameLoses[i]);
        ui->availGamesTable->setItem(i, 3, availGameID[i]);
        ui->availGamesTable->item(i,2)->setTextAlignment(Qt::AlignCenter);
        ui->availGamesTable->setItem(i, 4, availGameTies[i]);
        ui->availGamesTable->item(i,4)->setTextAlignment(Qt::AlignCenter);
        ui->availGamesTable->setRowHidden(i, true);
    }

    ui->sendMsgButton->setEnabled(false);
    ui->win_0->setVisible(false);
    ui->win_1->setVisible(false);
    ui->win_2->setVisible(false);
    ui->win_3->setVisible(false);
    ui->win_4->setVisible(false);
    ui->win_5->setVisible(false);
    ui->win_6->setVisible(false);
    ui->win_7->setVisible(false);


    // Create an instance of network setup thread
    networkSetup *workerThread = new networkSetup;
    // Connect our signal and slot
    connect(workerThread, SIGNAL(networkStatus(int)),
                          SLOT(onOpenNetwork(int)));
    // Setup callback for cleanup when it finishes
    connect(workerThread, SIGNAL(finished()),
            workerThread, SLOT(deleteLater()));
    workerThread->start(); // This invokes WorkerThread::run in a new thread
}

bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    //takes over event filter so we can handle keypress of enter and tab in the chat box
    if (object == ui->sendChatBox && event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->key() == Qt::Key_Return || keyEvent->key() == Qt::Key_Enter)
        {
            // Special tab handling
            if (ui->sendMsgButton->isEnabled() == true) {
                on_sendMsgButton_clicked();
            }
            return true;
        }
        else if (keyEvent->key() == Qt::Key_Tab) {
            ui->sendMsgButton->setFocus();
            return true;
        }
        else //not something we want to override
        {
            return QMainWindow::eventFilter(object, event);
        }
    }
    else //not something we want to override
    {
        return QMainWindow::eventFilter(object, event);
    }
}

//gets called by network setup thread
void MainWindow::onNetworkStatusChanged(int stat){
    if (stat >= 0) {
        ui->statusBar->showMessage("Connected to server");
    }
    else {
        networkFailure();
    }
}

//sets focus on password when someone hits tab
void MainWindow::gotoPassword() {
    ui->passWordField->setFocus();
}

//on successful network connection, called by thread
void MainWindow::onOpenNetwork(int stat) {
    if (stat >= 0) {
        ui->statusBar->showMessage("Connected to server");
        networkError = false;
        ui->reconectButton->setVisible(false);
        ui->reconectButton->setEnabled(true);
        ui->registerButton->setEnabled(true);
        ui->loginButton->setEnabled(true);
        gData::running = true;
        //keep alive thread initialzation
        keepAlive *keepAliveWorker = new keepAlive;
        // Connect our signal and slot
        connect(keepAliveWorker, SIGNAL(networkFailure()),
                SLOT(onNetworkFailure()));
        // Setup callback for cleanup when it finishes
        connect(keepAliveWorker, SIGNAL(finished()),
                keepAliveWorker, SLOT(deleteLater()));
        keepAliveWorker->start(); // This invokes run in a new thread

    }
    else {
        //failed connecting to server so disable buttons and show reconnect
        ui->reconectButton->setVisible(true);
        ui->reconectButton->setEnabled(true);
        ui->registerButton->setEnabled(false);
        ui->loginButton->setEnabled(false);
        QMessageBox messageBox;
        networkError = true;
        ui->statusBar->showMessage("Error connecting to server", 0);
        messageBox.critical(0,"Error","Could not connect to server.\nUnder the file menu, you can specify a server.\nOr click the reconnect button to try connecting again.");
    }
}

//called by register/login module upon getting result from server
void MainWindow::onLoginRegisterChanged(int action, int status) {
    //register: user exits
    if (status == 0 && action == 1) {
        QMessageBox messageBox;
        ui->statusBar->showMessage("Register: User already exists", 0);
        messageBox.critical(0,"Error","User already exists on server");
    }
    //login: no such user
    else if (status == 0 && action == 2) {
        QMessageBox messageBox;
        ui->statusBar->showMessage("Login: No such user exists", 0);
        messageBox.critical(0,"Error","No such user exists on server");
    }
    //login: invalid password
    else if (status == 2 && action == 2) {
        QMessageBox messageBox;
        ui->statusBar->showMessage("Login: Invalid Password", 0);
        messageBox.critical(0,"Error","Invalid password for " + ui->userNameField->text());
    }
    //login: server is full
    else if (status == 3 && action == 2) {
        ui->statusBar->showMessage("Login: Server is full", 0);
    }

    //login: successful so setup gui stuff
    else if (status ==1 && action == 2) {
        ui->loginButton->setText("Login");
        ui->loginButton->setDisabled(false);
        ui->statusBar->showMessage("Logged in", 0);
        ui->loggedInFrame->setVisible(true);
        gData::loggedIn = true;
        ui->loginScreen->setVisible(false);
        ui->gameFrame->setVisible(true);
        ui->labelUsername->setText("Welcome " + ui->userNameField->text());
        ui->scoreTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        //scoreboard module thread
        listScores *scores = new listScores;
        connect(scores, SIGNAL(updateScores(char **, int **, int **, int **)),
                              SLOT(onScore(char **, int **, int **, int **)));
        connect(scores, SIGNAL(networkFailure()),
                              SLOT(onNetworkFailure()));
        // Setup callback for cleanup when it finishes
        connect(scores, SIGNAL(finished()),
                scores, SLOT(deleteLater()));
        // Run, Forest, run!
        scores->start(); // This invokes run in a new thread
        return;
    }
    //Registration successful, so call login immediately
    else if (status == 1 && action == 1) {
        ui->registerButton->setText("Register");
        ui->registerButton->setDisabled(false);
        ui->statusBar->showMessage("Registered User");
        Register_Login(2); // logs users in next
    }
    //re-enables the previously disabled button to prevent many clicks
    if (action == 1) {
        ui->registerButton->setText("Register");
        ui->registerButton->setDisabled(false);

    }
    else if (action == 2) {
        ui->loginButton->setText("Login");
        ui->loginButton->setDisabled(false);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loginButton_clicked()
{
    //this if statement only occurs since we allow "enter" in password to hit the login button
    if (ui->loginButton->isEnabled() == false) {
        return;
    }
    if (gData::ConnectSocket == INVALID_SOCKET) {
        QMessageBox messageBox;
        messageBox.critical(0,"Try Again","Still attempting to connect to the server, please try again shortly");
        return;
    }
    ui->loginButton->setDisabled(true);
    ui->statusBar->showMessage("Logging in...");
    ui->loginButton->setText("Loading..");
    Register_Login(gData::LOGIN_MODULE);
}


//called by score thread to display latest scores
void MainWindow::onScore(char **username, int **wins, int **loses, int **ties) {

    //no available scores
    if (*wins[0] == -1) {
        //if there were previous games it needs to zero them out
        for (int i = 0; i < gData::numOfTopScores ; i++ ) {
            tableUserName[i]->setText("");
            tableWins[i]->setText("");
            tableLoses[i]->setText("");
            tableTies[i]->setText("");

            ui->scoreTable->setRowHidden(i, true);
        }
        tableUserName[0]->setText("No Users yet");
        tableWins[0]->setText("-");
        tableLoses[0]->setText("-");
        tableTies[0]->setText("-");
    }

    //add each available top score
    for (int i = 0; i < gData::numOfTopScores ; i++ ) {
        if (*wins[i] != -1) {
            tableUserName[i]->setText(QString::fromUtf8(username[i]));
            tableWins[i]->setText(QString::number(*wins[i]));
            tableLoses[i]->setText(QString::number(*loses[i]));
            tableTies[i]->setText(QString::number(*ties[i]));
            ui->scoreTable->setRowHidden(i, false);

        }
        else {
            //not available top score so zero it
            tableUserName[i]->setText("");
            tableWins[i]->setText("");
            tableLoses[i]->setText("");
            tableTies[i]->setText("");
            ui->scoreTable->setRowHidden(i, true);
        }
    }
    //updates scoreboard view
    ui->scoreTable->viewport()->update();
}

//someone clicked register button
void MainWindow::on_registerButton_clicked()
{
    if (gData::ConnectSocket == INVALID_SOCKET) {
        QMessageBox messageBox;
        messageBox.critical(0,"Try Again","Still attempting to connect to the server, please try again shortly");
        return;
    }
    ui->registerButton->setDisabled(true);
    ui->registerButton->setText("Loading..");
    ui->statusBar->showMessage("Attempting to register user...");
    Register_Login(gData::REGISTER_MODULE);
}

//overrides the standard "Red x button" so we can prompt user
void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Tic-Tac-Toe",
                                                                tr("Are you sure you want to quit?\n"),
                                                                QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        //they really wanna leave so will shut down everything
        gData::isRunning.lock();
        gData::gameRunning = false;
        gData::isRunning.unlock();
        gData::running = false;
        gData::loggedIn = false;
        gData::findingGames = false;
        logoutOrDelete *logDelModule = new logoutOrDelete;
        // Connect our signal and slot
        connect(logDelModule, SIGNAL(networkFailure()),
                              SLOT(onNetworkFailure()));
        connect(logDelModule, SIGNAL(quitStatus()),
                              SLOT(onQuitStatus()));
        // Setup callback for cleanup when it finishes
        connect(logDelModule, SIGNAL(finished()),
                logDelModule, SLOT(deleteLater()));
        // Run, Forest, run!
        logDelModule->setAction(gData::QUIT);
        logDelModule->start(); // This invokes run in a new thread
    }
}


//shuts down game on network failure
void MainWindow::networkFailure() {
    if (networkError == false ) {
    networkError = true;
    QMessageBox messageBox;
    messageBox.critical(0,"Error","Unable to connect to server. Exiting...");
    closesocket(gData::ConnectSocket);
    gData::running = false;
    WSACleanup();
    Sleep(200);
    exit(1);
    }
}

//checks login/register username/password and hashes password
void MainWindow::Register_Login(int action) {
    //checks for blank username
    if(ui->userNameField->text().length() == 0) {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Username cannot be blank");
        if (action == 2) {
            ui->loginButton->setText("Login");
            ui->loginButton->setDisabled(false);
            ui->statusBar->showMessage("Connected to server", 0);
        }
        else if (action == 1) {
            ui->registerButton->setText("Register");
            ui->registerButton->setDisabled(false);
            ui->statusBar->showMessage("Connected to server", 0);
        }
       return;
    }
    //checks to make sure username isnt longer than allowed length
    if(ui->userNameField->text().length() > 32 ) {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Username cannot be longer than 32 characters");
        if (action == 2) {
            ui->loginButton->setText("Login");
            ui->loginButton->setDisabled(false);
            ui->statusBar->showMessage("Connected to server", 0);
        }
        else if (action == 1) {
            ui->registerButton->setText("Register");
            ui->registerButton->setDisabled(false);
            ui->statusBar->showMessage("Connected to server", 0);
        }
        return ;
    }

    //makes sure password isnt blank
    if(ui->passWordField->text().length() == 0) {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Password cannot be blank");
        if (action == 2) {
            ui->loginButton->setText("Login");
            ui->loginButton->setDisabled(false);
            ui->statusBar->showMessage("Connected to server", 0);
        }
        else if (action == 1) {
            ui->registerButton->setText("Register");
            ui->registerButton->setDisabled(false);
            ui->statusBar->showMessage("Connected to server", 0);
        }
        return;
    }

    //hashes password with md5
    QString hashedPassword = QString(QCryptographicHash::hash((ui->passWordField->text().toStdString().c_str()),QCryptographicHash::Md5).toHex());
    //sets up buffer to send to server
    int bufferLength = sizeof(int)+sizeof(int)+ui->userNameField->text().length()+1+sizeof(int)+hashedPassword.length()+1;

    char *sendbuf = new char[bufferLength];
    int userNameLengthPos = sizeof(int);
    int userNamePos = sizeof(int)*2;
    int passwordLengthPos = userNamePos+ ui->userNameField->text().length()+1+1;
    int passwordPos = passwordLengthPos+sizeof(int);

    sendbuf[0] = action;
    sendbuf[userNameLengthPos] = ui->userNameField->text().length()+1;
    memcpy(sendbuf + (sizeof(int)*2), ui->userNameField->text().toStdString().c_str(), ui->userNameField->text().length()+1);
    sendbuf[passwordLengthPos-1] = '\0';

    //copies to globalData so other methods can use username later:
    gData::username = new char[ui->userNameField->text().length()+1];
    strcpy(gData::username,ui->userNameField->text().toStdString().c_str());
    gData::username[ui->userNameField->text().length()] = '\0'; //null terminator
    sendbuf[passwordLengthPos] = hashedPassword.length();
    memcpy(sendbuf + passwordPos, hashedPassword.toStdString().c_str(), hashedPassword.length());

    LoginRegisterModule *logRegMod = new LoginRegisterModule;
    // Connect our signal and slot for login/register thread
    connect(logRegMod, SIGNAL(LogRegStatus(int, int)),
                          SLOT(onLoginRegisterChanged(int, int)));
    connect(logRegMod, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));
    // Setup callback for cleanup when it finishes
    connect(logRegMod, SIGNAL(finished()),
            logRegMod, SLOT(deleteLater()));
    // Run, Forest, run!
    logRegMod->setVariables(bufferLength, sendbuf, action);
    logRegMod->start(); // This invokes run in a new thread
}

//network failure called by thread
void MainWindow::onNetworkFailure() {
    networkFailure();
}

//list game button clicked
void MainWindow::on_listGameButton_clicked()
{
       ui->loggedInFrame->setVisible(false);
       ui->selectGameFrame->setVisible(true);
       gData::findingGames = true;
       findGames *gameFinder = new findGames;
       ui->statusBar->showMessage("Finding available games", 0);
       // Connect our signal and slot for finding games
       connect(gameFinder, SIGNAL(updateAvailGames(int **, char **, int **, int **, int **)),
                             SLOT(onListGames(int **, char **, int **, int **, int **)));
       connect(gameFinder, SIGNAL(networkFailure()),
                             SLOT(onNetworkFailure()));
       connect(gameFinder, SIGNAL(noGames()),
                             SLOT(onNoGames()));

       // Setup callback for cleanup when it finishes
       connect(gameFinder, SIGNAL(finished()),
               gameFinder, SLOT(deleteLater()));
       // Run, Forest, run!
       gameFinder->start(); // This invokes run in a new thread
}

void MainWindow::onNoGames() {
    //no available games
        QMessageBox messageBox;
        ui->statusBar->showMessage("No Available Games", 0);
        messageBox.critical(0,"Error","There are no available games, please create your own or try again later");
        on_goBackButton_clicked();
}

//list games available called by listgame thread
void MainWindow::onListGames(int **gameIds, char **username, int **wins, int **loses, int **ties) {
    for (int i = 0; i < gData::numOfGamesListing ; i++ ) {
        if (*gameIds[i] != -1) { //displays game info in table
            availGameUserName[i]->setText(QString::fromUtf8(username[i]));
            availGameWins[i]->setText(QString::number(*wins[i]));
            availGameLoses[i]->setText(QString::number(*loses[i]));
            availGameID[i]->setText(QString::number(*gameIds[i]));
            availGameTies[i]->setText(QString::number(*ties[i]));
            ui->availGamesTable->setRowHidden(i, false);
        }
        else { //no game here so null them on display
            availGameUserName[i]->setText("");
            availGameWins[i]->setText("");
            availGameLoses[i]->setText("");
            availGameTies[i]->setText("");
            availGameID[i]->setText("-1");
            ui->availGamesTable->setRowHidden(i, true);
        }
    }
    ui->statusBar->showMessage("Found Available Games", 0);
    ui->availGamesTable->viewport()->update();

}

//user clicks create game button
void MainWindow::on_createGameButton_clicked()
{
    ui->createGameButton->setEnabled(false);
    ui->createGameButton->setText("Creating Game");
    createGame *gameCreator = new createGame;
    ui->statusBar->showMessage("Creating new game...", 0);
    // Connect our signal and slot for creating game thread
    connect(gameCreator, SIGNAL(createdGame(int)),
                          SLOT(onCreatedGame(int)));
    connect(gameCreator, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));

    // Setup callback for cleanup when it finishes
    connect(gameCreator, SIGNAL(finished()),
            gameCreator, SLOT(deleteLater()));
    // Run, Forest, run!
    gameCreator->start(); // This invokes in a new thread
}

//called by create game thread
void MainWindow::onCreatedGame(int gameID) {
    ui->statusBar->showMessage("Game created, waiting for second player...", 0);
    ui->loggedInFrame->setVisible(false);
    ui->createGameButton->setEnabled(true);
    ui->createGameButton->setText("Create Game");
    gData::gID = gameID;
    ui->tictactoeFrame->setVisible(true);
    gData::gameMaker = 1; // set this to my turn since i made the game

    pollOnCreatedGame *gamePoller = new pollOnCreatedGame;
    // Connect our signal and slot and polls for other player
    connect(gamePoller, SIGNAL(secondPlayerJoined(char *, int , int, int)),
                          SLOT(onGamePoller(char *, int, int, int)));
    connect(gamePoller, SIGNAL(statusUpdate(bool)),
                          SLOT(onGamePollerStatus(bool)));
    connect(gamePoller, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));

    // Setup callback for cleanup when it finishes
    connect(gamePoller, SIGNAL(finished()),
            gamePoller, SLOT(deleteLater()));
    // Run, Forest, run!
    gamePoller->start(); // This invokes run in a new thread

}
//called by gamepoller thread
void MainWindow::onGamePollerStatus(bool running){
    if (gData::foundOtherPlayer == true) { //other player found or went back
        return;
    }
    if (running == true ) {
        ui->statusBar->showMessage("Haven't found a second player yet...", 0);
    }
    else {
        ui->statusBar->showMessage("Checking for second player..", 0);
    }
}

//called by game poller thread if successful
void MainWindow::onGamePoller(char *userName, int wins, int loses, int ties) {

    if (gData::gameMaker == 1) {
        ui->statusBar->showMessage("Second player joined!");
    }
    gData::isRunning.lock();
    gData::gameRunning = true;
    gData::isRunning.unlock();
    gData::otherPlayer = userName;
    if (gData::gameMaker == 1) { //player that makes the game is always x and goes first
        ui->labelPlayersTurn->setText("Your turn, make a move:\nYou are 'X'");
    }
    else {
        ui->labelPlayersTurn->setText(QString(gData::otherPlayer)+"'s turn\nYou are 'O'");
    }
    //sets hand shape on the objects
    Qt::CursorShape hand;
    hand = Qt::OpenHandCursor;
    ui->tictactoe_0->setCursor(hand);
    ui->tictactoe_1->setCursor(hand);
    ui->tictactoe_2->setCursor(hand);
    ui->tictactoe_3->setCursor(hand);
    ui->tictactoe_4->setCursor(hand);
    ui->tictactoe_5->setCursor(hand);
    ui->tictactoe_6->setCursor(hand);
    ui->tictactoe_7->setCursor(hand);
    ui->tictactoe_8->setCursor(hand);
    //shows opponents data
    QString print = QString("Opponent: "+QString(userName)+"\nOpponent's Wins: %1\nOpponent's Loses:  %2\nOpponent's Ties:  %3").arg(wins).arg(loses).arg(ties);
    ui->labelPlayerVS->setText(print);

    ui->labelPlayersTurn->setVisible(true);
    ui->labelPlayerVS->setVisible(true);
    //initate a thread to POLL For game updates
    gamePolling *gameStatus = new gamePolling;
    // Connect our signal and slot
    connect(gameStatus, SIGNAL(updateGameBoard(int, int , int )),
                          SLOT(onUpdateGameBoard(int , int , int)));
    connect(gameStatus, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));
    // Setup callback for cleanup when it finishes
    connect(gameStatus, SIGNAL(finished()),
            gameStatus, SLOT(deleteLater()));
    // Run, Forest, run!
    gameStatus->start(); // This invokes run in a new thread
    //initate a thread to POLL for messages
   messengerModule *messenger = new messengerModule;
    // Connect our signal and slot
    connect(messenger, SIGNAL(messageUpdate(char *)),
                          SLOT(onMessage(char *)));
    connect(messenger, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));
    // Setup callback for cleanup when it finishes
    connect(messenger, SIGNAL(finished()),
            messenger, SLOT(deleteLater()));
    // Run, Forest, run!
    messenger->start(); // This invokes run in a new thread
}

//updating gameboard data, called by game status thread
void MainWindow::onUpdateGameBoard(int turn, int gameOver, int winType){
    gData::boardLock.lock();

    if (turn == -2) {
        //other player disconnected
        //need to close outta game yo
        gData::isRunning.lock();
        gData::gameRunning = false;
        gData::isRunning.unlock();
        ui->statusBar->showMessage("Other player has left!");
        QMessageBox messageBox;
        gData::gameMaker = 0;
        QString temp = QString(gData::otherPlayer);
        messageBox.critical(0,"Error", temp + " has left the game.\nExiting game too!");

        ui->loggedInFrame->setVisible(true);
        ui->tictactoeFrame->setVisible(false);
        ui->labelPlayersTurn->setVisible(false);
        ui->labelPlayerVS->setVisible(false);
        ui->sendChatBox->clear();
        ui->displayChat->clear();
        //resets icons
        ui->tictactoe_0->setIcon(QPixmap());
        ui->tictactoe_1->setIcon(QPixmap());
        ui->tictactoe_2->setIcon(QPixmap());
        ui->tictactoe_3->setIcon(QPixmap());
        ui->tictactoe_4->setIcon(QPixmap());
        ui->tictactoe_5->setIcon(QPixmap());
        ui->tictactoe_6->setIcon(QPixmap());
        ui->tictactoe_7->setIcon(QPixmap());
        ui->tictactoe_8->setIcon(QPixmap());
        gData::boardLock.unlock();
        return;
    }

    for (int i = 0; i < 9; i++) {
        //if 1, that means x marked the spot
        if (gData::gameBoard[i] == 1) {
            QPixmap pixmap(":/images/x.png");
            QIcon ButtonIcon(pixmap);
            if (i == 0) ui->tictactoe_0->setIcon(ButtonIcon);
            else if (i == 1) ui->tictactoe_1->setIcon(ButtonIcon);
            else if (i == 2) ui->tictactoe_2->setIcon(ButtonIcon);
            else if (i == 3) ui->tictactoe_3->setIcon(ButtonIcon);
            else if (i == 4) ui->tictactoe_4->setIcon(ButtonIcon);
            else if (i == 5) ui->tictactoe_5->setIcon(ButtonIcon);
            else if (i == 6) ui->tictactoe_6->setIcon(ButtonIcon);
            else if (i == 7) ui->tictactoe_7->setIcon(ButtonIcon);
            else if (i == 8) ui->tictactoe_8->setIcon(ButtonIcon);
        }
        //if 2 that means o marked the spot
        else if (gData::gameBoard[i] == 2) {
            QPixmap pixmap(":/images/o.png");
            QIcon ButtonIcon(pixmap);
            if (i == 0) ui->tictactoe_0->setIcon(ButtonIcon);
            else if (i == 1) ui->tictactoe_1->setIcon(ButtonIcon);
            else if (i == 2) ui->tictactoe_2->setIcon(ButtonIcon);
            else if (i == 3) ui->tictactoe_3->setIcon(ButtonIcon);
            else if (i == 4) ui->tictactoe_4->setIcon(ButtonIcon);
            else if (i == 5) ui->tictactoe_5->setIcon(ButtonIcon);
            else if (i == 6) ui->tictactoe_6->setIcon(ButtonIcon);
            else if (i == 7) ui->tictactoe_7->setIcon(ButtonIcon);
            else if (i == 8) ui->tictactoe_8->setIcon(ButtonIcon);
        }
    }
    Qt::CursorShape arrow;
    Qt::CursorShape hand;
    hand = Qt::OpenHandCursor;
    arrow = Qt::ArrowCursor;
    //sets hand/vs arrow for hoverover
    if (turn == 1 )  {
        ui->tictactoe_0->setCursor(hand);
        ui->tictactoe_1->setCursor(hand);
        ui->tictactoe_2->setCursor(hand);
        ui->tictactoe_3->setCursor(hand);
        ui->tictactoe_4->setCursor(hand);
        ui->tictactoe_5->setCursor(hand);
        ui->tictactoe_6->setCursor(hand);
        ui->tictactoe_7->setCursor(hand);
        ui->tictactoe_8->setCursor(hand);
        if (gData::gameMaker == 1) {
            ui->labelPlayersTurn->setText("Your turn, make a move:\nYou are 'X'");
        }
        else {
            ui->labelPlayersTurn->setText("Your turn, make a move:\nYou are 'O'");
        }
        ui->statusBar->showMessage("Your turn, make a move!");
    }
    else {
        ui->tictactoe_0->setCursor(arrow);
        ui->tictactoe_1->setCursor(arrow);
        ui->tictactoe_2->setCursor(arrow);
        ui->tictactoe_3->setCursor(arrow);
        ui->tictactoe_4->setCursor(arrow);
        ui->tictactoe_5->setCursor(arrow);
        ui->tictactoe_6->setCursor(arrow);
        ui->tictactoe_7->setCursor(arrow);
        ui->tictactoe_8->setCursor(arrow);
        if (gData::gameMaker == 1) {
            ui->labelPlayersTurn->setText(QString(gData::otherPlayer)+"'s turn\nYou are 'X'");
        }
        else {
            ui->labelPlayersTurn->setText(QString(gData::otherPlayer)+"'s turn\nYou are 'O'");
        }
        ui->statusBar->showMessage("Waiting on your opponent to make a move!");
    }
    gData::whosTurn = turn;

    //check if won here
    if (gameOver != -1) { //-1 = not over
        gData::isRunning.lock();
        gData::gameRunning = false;
        gData::isRunning.unlock();

        if (gameOver ==0 ) { //you won
            ui->statusBar->showMessage("You won the game!");
            ui->labelPlayersTurn->setText("Congratulations! You Won!");
        }
        else if (gameOver == 1 ){ //other side won
            ui->statusBar->showMessage("You lost the game!");
            QString temp = QString("You lost! %1 won the game!").arg(gData::otherPlayer);
            ui->labelPlayersTurn->setText(temp);
        }
        else { //tie
            ui->statusBar->showMessage("You both tied!");
            ui->labelPlayersTurn->setText("Congratulations! You Tied!");
        }
        //shows win type
        if (winType ==0) (ui->win_0->setVisible(true));
        else if (winType ==1) (ui->win_1->setVisible(true));
        else if (winType ==2) (ui->win_2->setVisible(true));
        else if (winType ==3) (ui->win_3->setVisible(true));
        else if (winType ==4) (ui->win_4->setVisible(true));
        else if (winType ==5) (ui->win_5->setVisible(true));
        else if (winType ==6) (ui->win_6->setVisible(true));
        else if (winType ==7) (ui->win_7->setVisible(true));
        gData::gameMaker = 0;
        ui->quitGameButton->setText("Return to menu");
    }
    gData::boardLock.unlock();

}

//user clicked go back button
void MainWindow::on_goBackButton_clicked() {

    ui->loggedInFrame->setVisible(true);
    ui->selectGameFrame->setVisible(false);
    gData::findingGames = false;
    ui->statusBar->showMessage("Connected to server", 0);
}

//user selected to change  server on drop down menu
void MainWindow::on_actionChange_Server_triggered()
{
    bool ok;
    int result = -1;
    QString ipAddr;
    QString port;
    do {
        //gets IP of server
        ipAddr = QInputDialog::getText(this, tr("Enter IP of Server"),tr("IP Address of Server:"), QLineEdit::Normal,"", &ok);
        std::stringstream s(ipAddr.toStdString());
        int a,b,c,d; //to store the 4 ints
        char ch; //to temporarily store the '.'
        s >> a >> ch >> b >> ch >> c >> ch >> d;
        //checks if a valid IP address
        if (a <=255 && b <= 255 && c <= 255 && d <= 255) { result = 1;}
        if (ok == true && result != 1) {
            QMessageBox messageBox;
            messageBox.critical(0,"Error", ipAddr + " is not a valid IP address! Try Again");
        }
    }
    while ((result == -1) && (ok == true));

    result = -1;
    //gets port #
    do {
        port = QInputDialog::getText(this, tr("Enter Server Port"),tr("Server Port:"), QLineEdit::Normal,"", &ok);
        std::stringstream s(port.toStdString());
        int a;
        s >> a;
        if (a > 0 && a < 65535) { result = 1; };
        if (ok == true && result != 1) {
            QMessageBox messageBox;
            messageBox.critical(0,"Error", port + " is not a valid port number! Try Again");
        }
    }
    while ((result == -1) && (ok == true));
    if (ok == true) {//connects to new server
        ui->statusBar->showMessage("Connecting to server...");
        gData::port = new char[port.toStdString().length()];
        strcpy(gData::port,port.toStdString().c_str());
        gData::ip = new char[ipAddr.toStdString().length()];
        strcpy(gData::ip,ipAddr.toStdString().c_str());
        networkSetup *workerThread = new networkSetup;
        // Connect our signal and slot for network status
        connect(workerThread, SIGNAL(networkStatus(int)),
                              SLOT(onOpenNetwork(int)));
        // Setup callback for cleanup when it finishes
        connect(workerThread, SIGNAL(finished()),
                workerThread, SLOT(deleteLater()));
        // Run, Forest, run!
        workerThread->start(); // This invokes run in a new thread
    }
}

//user is typing a message!!!
void MainWindow::on_sendChatBox_textChanged()
{
    //shows send message only if a message exists here
    if (ui->sendChatBox->toPlainText().length() >= 1 ) { ui->sendMsgButton->setEnabled(true); }
    if (ui->sendChatBox->toPlainText().length() == 0 ) { ui->sendMsgButton->setEnabled(false); }

    //deletes the last character if they go above MAX_CHARS characters
    if(ui->sendChatBox->toPlainText().length() > gData::MAX_CHARS) {
    ui->sendChatBox->setText(ui->sendChatBox->toPlainText().left(gData::MAX_CHARS));
    QTextCursor tmpCursor = ui->sendChatBox->textCursor();
    tmpCursor.setPosition(gData::MAX_CHARS);
    ui->sendChatBox->setTextCursor(tmpCursor);
}
    QString temp = QString("%1/%2").arg(ui->sendChatBox->toPlainText().length()).arg(gData::MAX_CHARS);
    ui->labelMsgSize->setText(temp);

}

//user clicked on send message button
void MainWindow::on_sendMsgButton_clicked()
{
    gData::isRunning.lock();
    //can only snd message if second player exists WHILE game is running
    if (gData::gameRunning != true) {
        gData::isRunning.unlock();
        QMessageBox messageBox;
        ui->statusBar->showMessage("Could NOT send message!", 0);
        messageBox.setText("No other player yet! Can't send message");
        messageBox.exec();
        return;
    }
    gData::isRunning.unlock();

    //apends what user sent o his own chat box
    QString temp("<");
    temp.append(gData::username);
    temp.append(">:  ");
    temp.append(ui->sendChatBox->toPlainText());
    char *newMsg = new char[ui->sendChatBox->toPlainText().toStdString().length()+1];
    memcpy(newMsg,ui->sendChatBox->toPlainText().toStdString().c_str(),ui->sendChatBox->toPlainText().length()+1);
    newMsg[ui->sendChatBox->toPlainText().toStdString().length()] = '\0';
    ui->displayChat->append(temp);
    ui->sendChatBox->clear();
    msgSender *Message = new msgSender;
    // Connect our signal and slot for sending message
    connect(Message, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));

    // Setup callback for cleanup when it finishes
    connect(Message, SIGNAL(finished()),
            Message, SLOT(deleteLater()));
    // Run, Forest, run!
    Message->setMsgPtr(newMsg);
    Message->start(); // This invokes run in a new thread
}

//user pressed reconnect button - only shows up at starting program
//and is unable to connect to the server
void MainWindow::on_reconectButton_clicked()
{
    ui->reconectButton->setEnabled(false);
    ui->statusBar->showMessage("Connecting to server...");
    networkSetup *workerThread = new networkSetup;
    // Connect our signal and slot to network status
    connect(workerThread, SIGNAL(networkStatus(int)),
                          SLOT(onOpenNetwork(int)));
    // Setup callback for cleanup when it finishes
    connect(workerThread, SIGNAL(finished()),
            workerThread, SLOT(deleteLater()));
    // Run, Forest, run!
    workerThread->start(); // This invokes run in a new thread
}

//user selected an available game to join
void MainWindow::on_availGamesTable_doubleClicked(const QModelIndex &index)
{
    ui->availGamesTable->setEnabled(false);
    ui->statusBar->showMessage("Attempting to join game...");
    ui->availGamesTable->selectRow(index.row());
    gData::gID = ui->availGamesTable->item(index.row(), 3)->text().toInt();
    joinExistingGame *joinGame = new joinExistingGame;
    // Connect our signal and slot for trying to join the existing game
    connect(joinGame, SIGNAL(joinGameStatus(char*, int , int , int)),
                          SLOT(onUpdateJoinStatus(char* , int , int, int)));
    connect(joinGame, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));

    // Setup callback for cleanup when it finishes
    connect(joinGame, SIGNAL(finished()),
            joinGame, SLOT(deleteLater()));
    // Run, Forest, run!
    joinGame->start(); // This invokes run in a new thread
}

//called by join game module
void MainWindow::onUpdateJoinStatus(char *username, int wins, int loses, int ties) {
    gData::findingGames = false;

    //resets available games as it doesnt currently poll again
    for (int i = 0; i < gData::numOfGamesListing;  i++) {
        ui->availGamesTable->setRowHidden(i, true);
    }
    if (loses == -1) { // cant join game!! someone else probably joined before we did
        ui->statusBar->showMessage("Unable to join that game");
        QMessageBox messageBox;
        messageBox.setText("Someone else has already joined that game.");
        messageBox.exec();
        ui->availGamesTable->setEnabled(true);
        return;

    }
    //join game success!!
    ui->statusBar->showMessage("Joined the game");
    ui->loggedInFrame->setVisible(false);
    ui->tictactoeFrame->setVisible(true);
    ui->selectGameFrame->setVisible(false);
    ui->availGamesTable->setEnabled(true);

    gData::gameRunning = true;
    gData::otherPlayer = username;
    ui->labelPlayersTurn->setText(QString(gData::otherPlayer)+"'s turn\nYou are 'O'");
    Qt::CursorShape arrow;
    arrow = Qt::ArrowCursor;
    ui->tictactoe_0->setCursor(arrow);
    ui->tictactoe_1->setCursor(arrow);
    ui->tictactoe_2->setCursor(arrow);
    ui->tictactoe_3->setCursor(arrow);
    ui->tictactoe_4->setCursor(arrow);
    ui->tictactoe_5->setCursor(arrow);
    ui->tictactoe_6->setCursor(arrow);
    ui->tictactoe_7->setCursor(arrow);
    ui->tictactoe_8->setCursor(arrow);
    QString print = QString("Opponent: "+QString(username)+"\nOpponent's Wins: %1\nOpponent's Loses:  %2\nOpponent's Ties: %3").arg(wins).arg(loses).arg(ties);
    ui->labelPlayerVS->setText(print);

    ui->labelPlayersTurn->setVisible(true);
    ui->labelPlayerVS->setVisible(true);
    //initate a thread to POLL For game updates
    gamePolling *gameStatus = new gamePolling;
    // Connect our signal and slot
    connect(gameStatus, SIGNAL(updateGameBoard(int, int , int )),
                          SLOT(onUpdateGameBoard(int , int , int)));
    connect(gameStatus, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));

    // Setup callback for cleanup when it finishes
    connect(gameStatus, SIGNAL(finished()),
            gameStatus, SLOT(deleteLater()));
    // Run, Forest, run!
    gameStatus->start(); // This invokes run in a new thread

    //initate a thread to POLL For chat messages
   messengerModule *messenger = new messengerModule;
    // Connect our signal and slot
    connect(messenger, SIGNAL(messageUpdate(char *)),
                          SLOT(onMessage(char *)));
    connect(messenger, SIGNAL(networkFailure()),
                          SLOT(onNetworkFailure()));

    // Setup callback for cleanup when it finishes
    connect(messenger, SIGNAL(finished()),
            messenger, SLOT(deleteLater()));
    // Run, Forest, run!
    messenger->start(); // This invokes run in a new thread
}

//called by poll message module
void MainWindow::onMessage(char *message){
    //appends message to chat box
    QString temp("<");
    temp.append(gData::otherPlayer);
    temp.append(">:  ");
    temp.append(message);
    delete[] message;
    ui->displayChat->append(temp);

}

//user clicked on delete account button
void MainWindow::on_deleteGameButton_clicked()
{
    QMessageBox msgBox;
    msgBox.addButton(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setText("Are you sure you want to delete your account?\nThis will remove it from the game server!\nYou can only delete your account if you do not have multiple sessions logged in.");
    msgBox.setWindowTitle("Confirmation Window");
    int selection = msgBox.exec();
    if(selection == QMessageBox::Yes) //yep they wanna delete their acct
    {
        logoutOrDelete *logDelModule = new logoutOrDelete;
        // Connect our signal and slot
        connect(logDelModule, SIGNAL(networkFailure()), SLOT(onNetworkFailure()));
        connect(logDelModule, SIGNAL(deleted(int)), SLOT(onDeleteStatus(int)));
        // Setup callback for cleanup when it finishes
        connect(logDelModule, SIGNAL(finished()),
                logDelModule, SLOT(deleteLater()));
        // Run, Forest, run!

        ui->statusBar->showMessage("Trying to delete account...");
        logDelModule->setAction(gData::DELETE_MODULE);
        logDelModule->start(); // This invokes run in a new thread

    }
}

//called by delete/logout/quit game module
void MainWindow::onDeleteStatus(int deleted) {
    if (deleted == 3) { //that means leave a running game

        //resets gameboard data
        gData::gameMaker = 0;
        ui->labelPlayersTurn->setVisible(false);
        ui->labelPlayerVS->setVisible(false);
        ui->tictactoeFrame->setVisible(false);
        ui->loggedInFrame->setVisible(true);
        ui->quitGameButton->setText("Quit Game");
        ui->statusBar->showMessage("Connected to server.");
        ui->sendChatBox->clear();
        ui->displayChat->clear();
        ui->win_0->setVisible(false);
        ui->win_1->setVisible(false);
        ui->win_2->setVisible(false);
        ui->win_3->setVisible(false);
        ui->win_4->setVisible(false);
        ui->win_5->setVisible(false);
        ui->win_6->setVisible(false);
        ui->win_7->setVisible(false);
        for (int i = 0; i < 9; i++ ) {
                gData::gameBoard[i] = 0;
            }
        ui->tictactoe_0->setIcon(QPixmap());
        ui->tictactoe_1->setIcon(QPixmap());
        ui->tictactoe_2->setIcon(QPixmap());
        ui->tictactoe_3->setIcon(QPixmap());
        ui->tictactoe_4->setIcon(QPixmap());
        ui->tictactoe_5->setIcon(QPixmap());
        ui->tictactoe_6->setIcon(QPixmap());
        ui->tictactoe_7->setIcon(QPixmap());
        ui->tictactoe_8->setIcon(QPixmap());
        return;
    }

    if (deleted == 1) { //means acct has been deleted
        ui->gameFrame->setVisible(false);
        ui->loggedInFrame->setVisible(false);
        ui->loginScreen->setVisible(true);
        ui->selectGameFrame->setVisible(false);
        ui->statusBar->showMessage("Your account has been deleted");
        ui->tictactoeFrame->setVisible(false);
        gData::running = false;
        gData::loggedIn = false;
        gData::isRunning.lock();
        gData::gameRunning = false;
        gData::isRunning.unlock();
    }
    else //couldnt delete acct
    {
        QMessageBox messageBox;
        ui->statusBar->showMessage("Could NOT delete account!", 0);
        messageBox.setText("You have another session logged in!\nYour account cannot be deleted if you have multiple sessions signed in");
        messageBox.exec();
    }
}

//user clicked logout button
void MainWindow::on_logOutButton_clicked()
{
    QMessageBox msgBox;
    msgBox.addButton(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setText("Are you sure you want to log out?\nThis will return to the main menu afterwards");
    msgBox.setWindowTitle("Confirmation Window");
    int selection = msgBox.exec();
    if(selection == QMessageBox::Yes)
    {
        //logging user out per their request
        ui->gameFrame->setVisible(false);
        ui->loginScreen->setVisible(true);
        ui->selectGameFrame->setVisible(false);
        ui->statusBar->showMessage("You have been logged out");
        ui->tictactoeFrame->setVisible(false);
        ui->loggedInFrame->setVisible(false);
        gData::isRunning.lock();
        gData::gameRunning = false;
        gData::isRunning.unlock();
        gData::loggedIn = false;
        logoutOrDelete *logDelModule = new logoutOrDelete;
        // Connect our signal and slot
        connect(logDelModule, SIGNAL(networkFailure()),
                              SLOT(onNetworkFailure()));
        // Setup callback for cleanup when it finishes
        connect(logDelModule, SIGNAL(finished()),
                logDelModule, SLOT(deleteLater()));
        // Run, Forest, run!
        logDelModule->setAction(gData::LOGOUT_MODULE);
        logDelModule->start(); // This invokes run in a new thread
    }

}

//called upon sending quit to server by thread
void MainWindow::onQuitStatus() {
    Sleep(500);
    exit(0);
}

//user selects quit from drop down menu
void MainWindow::on_actionQuit_triggered()
{
    QMessageBox msgBox;
    msgBox.addButton(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setText("Are you sure you want to quit?\nThis will log you out and exit the game");
    msgBox.setWindowTitle("Confirmation Window");
    int selection = msgBox.exec();
    if(selection == QMessageBox::Yes) //okay user wants to quit
    {
        gData::isRunning.lock();
        if (gData::gameRunning == true) {
            gData::foundOtherPlayer  = true; //makes poll on create stop
            logoutOrDelete *logDelModule = new logoutOrDelete;
            // Connect our signal and slot
            connect(logDelModule, SIGNAL(networkFailure()), SLOT(onNetworkFailure()));
            connect(logDelModule, SIGNAL(deleted(int)), SLOT(onDeleteStatus(int)));
            // Setup callback for cleanup when it finishes
            connect(logDelModule, SIGNAL(finished()),
                    logDelModule, SLOT(deleteLater()));
            // Run, Forest, run!
            logDelModule->setAction(gData::QUIT_GAME_MODULE);
            logDelModule->start(); // This invokes run in a new thread
            Sleep(600);
        }
        //tells other modules to close
        gData::gameRunning = false;
        gData::isRunning.unlock();
        gData::running = false;
        gData::loggedIn = false;
        gData::findingGames = false;

        //tells server we are quittnig
        logoutOrDelete *logDelModule = new logoutOrDelete;
        // Connect our signal and slot
        connect(logDelModule, SIGNAL(networkFailure()),
                              SLOT(onNetworkFailure()));
        connect(logDelModule, SIGNAL(quitStatus()),
                              SLOT(onQuitStatus()));
        // Setup callback for cleanup when it finishes
        connect(logDelModule, SIGNAL(finished()),
                logDelModule, SLOT(deleteLater()));
        // Run, Forest, run!
        logDelModule->setAction(gData::QUIT);
        logDelModule->start(); // This invokes run in a new thread
        return;
    }
}


//handles click from any tic-tac-toe board square
void MainWindow::boardHandler(int square) {

    if (gData::whosTurn == -1) { //not this users turn
        QMessageBox messageBox;
        ui->statusBar->showMessage("Whoa there, this isn't your turn yet!", 0);
        messageBox.critical(0,"Wait your turn","This isn't your turn!");
    }
    else if (gData::whosTurn == 0) { //server hasnt responded yet to our request to make a last move
        QMessageBox messageBox;
        ui->statusBar->showMessage("Server is still processing your last move!", 0);
        messageBox.critical(0,"Please wait","Server is still processing your move!");
    }
    //checks to see if the box has been clicked on already
    else if (gData::gameBoard[square] != 0) {
        QMessageBox messageBox;
        ui->statusBar->showMessage("That box has been marked already!", 0);
        messageBox.critical(0,"Invalid Selection","That box has been marked!");
        return;
    }
    else if (gData::whosTurn == 1 ) { //this means my turn!
        gData::whosTurn = 0;

        ui->statusBar->showMessage("Sending move to the server...");
        if (gData::gameMaker == 1) { //person that makes the game is always player 1 aka X
            ui->labelPlayersTurn->setText("Trying to send move to the server:\nYou are 'X'");
        }
        else
        {
            ui->labelPlayersTurn->setText("Trying to send move to the server:\nYou are 'O'");
        }

        sendMove *moveSender = new sendMove;
        // Connect our signal and slot to making move thread
        connect(moveSender, SIGNAL(moveUpdated(int,int)),
                              SLOT(onUpdatedMove(int,int)));
        connect(moveSender, SIGNAL(networkFailure()),
                              SLOT(onNetworkFailure()));
        // Setup callback for cleanup when it finishes
        connect(moveSender, SIGNAL(finished()),
                moveSender, SLOT(deleteLater()));
        // Run, Forest, run!
        moveSender->setAction(square);
        moveSender->start(); // This invokes run in a new thread


    }
}

//called by make move module upon answer from server
void MainWindow::onUpdatedMove(int status, int move) {
    //unable to make a move here
    if (status == -1) {
        QMessageBox messageBox;
        ui->statusBar->showMessage("Unable to make that move!", 0);
        messageBox.critical(0,"Invalid Selection","Could not select that spot");
        if (gData::whosTurn == 1) {
            if (gData::gameMaker == 1) {
                ui->labelPlayersTurn->setText("Your turn, make a move:\nYou are 'X'");
            }
            else {
                ui->labelPlayersTurn->setText("Your turn, make a move:\nYou are 'O'");
            }
        ui->statusBar->showMessage("Your turn, make a move!");
        }
        else {
            if (gData::gameMaker == 1) {
                ui->labelPlayersTurn->setText(QString(gData::otherPlayer)+"'s turn\nYou are 'X'");
            }
            else {
                ui->labelPlayersTurn->setText(QString(gData::otherPlayer)+"'s turn\nYou are 'O'");
            }
            ui->statusBar->showMessage("Waiting on your opponent to make a move!");
        }

    }
    else //move successful!! marks spot on board
    {
        if (gData::gameMaker == 1) {
            QPixmap pixmap(":/images/x.png");
            QIcon ButtonIcon(pixmap);
            if (move == 0) ui->tictactoe_0->setIcon(ButtonIcon);
            if (move == 1) ui->tictactoe_1->setIcon(ButtonIcon);
            if (move == 2) ui->tictactoe_2->setIcon(ButtonIcon);
            if (move == 3) ui->tictactoe_3->setIcon(ButtonIcon);
            if (move == 4) ui->tictactoe_4->setIcon(ButtonIcon);
            if (move == 5) ui->tictactoe_5->setIcon(ButtonIcon);
            if (move == 6) ui->tictactoe_6->setIcon(ButtonIcon);
            if (move == 7) ui->tictactoe_7->setIcon(ButtonIcon);
            if (move == 8) ui->tictactoe_8->setIcon(ButtonIcon);
        }
        else {
            QPixmap pixmap(":/images/o.png");
            QIcon ButtonIcon(pixmap);
            if (move == 0) ui->tictactoe_0->setIcon(ButtonIcon);
            if (move == 1) ui->tictactoe_1->setIcon(ButtonIcon);
            if (move == 2) ui->tictactoe_2->setIcon(ButtonIcon);
            if (move == 3) ui->tictactoe_3->setIcon(ButtonIcon);
            if (move == 4) ui->tictactoe_4->setIcon(ButtonIcon);
            if (move == 5) ui->tictactoe_5->setIcon(ButtonIcon);
            if (move == 6) ui->tictactoe_6->setIcon(ButtonIcon);
            if (move == 7) ui->tictactoe_7->setIcon(ButtonIcon);
            if (move == 8) ui->tictactoe_8->setIcon(ButtonIcon);
        }
        Qt::CursorShape arrow;
        arrow = Qt::ArrowCursor;

        ui->tictactoe_0->setCursor(arrow);
        ui->tictactoe_1->setCursor(arrow);
        ui->tictactoe_2->setCursor(arrow);
        ui->tictactoe_3->setCursor(arrow);
        ui->tictactoe_4->setCursor(arrow);
        ui->tictactoe_5->setCursor(arrow);
        ui->tictactoe_6->setCursor(arrow);
        ui->tictactoe_7->setCursor(arrow);
        ui->tictactoe_8->setCursor(arrow);
        // call server that youve made a move
        // switch display of info to about player 2
        ui->statusBar->showMessage("Waiting on your opponent to make a move!");
        if (gData::gameMaker == 1) {
            ui->labelPlayersTurn->setText("Your opponents turn, please wait:\nYou are 'X'");
        }
        else {
            ui->labelPlayersTurn->setText("Your opponents turn, please wait:\nYou are 'O'");
        }

    }

}

void MainWindow::on_tictactoe_0_clicked()
{
    boardHandler(0);
}


void MainWindow::on_tictactoe_1_clicked()
{
    boardHandler(1);
}

void MainWindow::on_tictactoe_2_clicked()
{
    boardHandler(2);
}

void MainWindow::on_tictactoe_3_clicked()
{
    boardHandler(3);
}

void MainWindow::on_tictactoe_4_clicked()
{
    boardHandler(4);
}

void MainWindow::on_tictactoe_5_clicked()
{
    boardHandler(5);
}

void MainWindow::on_tictactoe_6_clicked()
{
    boardHandler(6);
}

void MainWindow::on_tictactoe_7_clicked()
{
    boardHandler(7);
}

void MainWindow::on_tictactoe_8_clicked()
{
    boardHandler(8);
}


//user clicks quit game button
void MainWindow::on_quitGameButton_clicked()
{
    gData::isRunning.lock();
    //user is trying to leave a running game with a second player in it!!
    if (gData::gameRunning == true || gData::foundOtherPlayer == false)
    {
        QMessageBox msgBox;
        msgBox.addButton(QMessageBox::Yes);
        msgBox.addButton(QMessageBox::No);
        msgBox.setText("Are you sure you want to leave in the middle of a game?");
        msgBox.setWindowTitle("Confirmation Window");
        int selection = msgBox.exec();
        if(selection == QMessageBox::Yes)
        {
            gData::gameRunning = false;
            gData::isRunning.unlock();
            gData::playerSearch.lock();
            ui->displayChat->clear();
            ui->sendChatBox->clear();
            gData::foundOtherPlayer  = true; //makes poll on create stop
            logoutOrDelete *logDelModule = new logoutOrDelete;
            // Connect our signal and slot
            connect(logDelModule, SIGNAL(networkFailure()), SLOT(onNetworkFailure()));
            connect(logDelModule, SIGNAL(deleted(int)), SLOT(onDeleteStatus(int)));
            // Setup callback for cleanup when it finishes
            connect(logDelModule, SIGNAL(finished()),
                    logDelModule, SLOT(deleteLater()));
            // Run, Forest, run!

            ui->statusBar->showMessage("Left game successfully");
            logDelModule->setAction(gData::QUIT_GAME_MODULE);
            logDelModule->start(); // This invokes WorkerThread::run in a new thread
            gData::playerSearch.unlock();
            return;
        }
        else
        {
            gData::isRunning.unlock();
            return;
        }
    }
    //user can quit as the game is over so rests board
    gData::isRunning.unlock();
    ui->labelPlayersTurn->setVisible(false);
    ui->labelPlayerVS->setVisible(false);
    ui->tictactoeFrame->setVisible(false);
    ui->loggedInFrame->setVisible(true);
    ui->quitGameButton->setText("Quit Game");
    ui->statusBar->showMessage("Connected to server.");
    ui->sendChatBox->clear();
    ui->displayChat->clear();
    ui->win_0->setVisible(false);
    ui->win_1->setVisible(false);
    ui->win_2->setVisible(false);
    ui->win_3->setVisible(false);
    ui->win_4->setVisible(false);
    ui->win_5->setVisible(false);
    ui->win_6->setVisible(false);
    ui->win_7->setVisible(false);
    for (int i = 0; i < 9; i++ ) {
            gData::gameBoard[i] = 0;
        }
    ui->tictactoe_0->setIcon(QPixmap());
    ui->tictactoe_1->setIcon(QPixmap());
    ui->tictactoe_2->setIcon(QPixmap());
    ui->tictactoe_3->setIcon(QPixmap());
    ui->tictactoe_4->setIcon(QPixmap());
    ui->tictactoe_5->setIcon(QPixmap());
    ui->tictactoe_6->setIcon(QPixmap());
    ui->tictactoe_7->setIcon(QPixmap());
    ui->tictactoe_8->setIcon(QPixmap());
}
