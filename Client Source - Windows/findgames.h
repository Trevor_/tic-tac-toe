#ifndef FINDGAMES_H
#define FINDGAMES_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class findGames : public QThread {
    Q_OBJECT
    public:
        void run();

        signals:
        void networkFailure();
        void noGames();
        void updateAvailGames(int **gameIds, char **username, int **wins, int **loses, int **ties);
    private:
        int pollSocket();
};


#endif // FINDGAMES_H
