#ifndef LISTSCORES_H
#define LISTSCORES_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class listScores : public QThread {
    Q_OBJECT
    public:
        void run();

        signals:
        void networkFailure();
        void updateScores(char **username, int **wins, int **loses, int **ties);
    private:
        int pollSocket();
};


#endif // LISTSCORES_H
