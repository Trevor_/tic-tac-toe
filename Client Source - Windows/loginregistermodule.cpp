#include "loginregistermodule.h"
#include "globaldata.h"

void LoginRegisterModule::run()
{
    gData::socketMutex.lock(); //locks socket from other threads

    //sends size of next message to server
    int result = send(gData::ConnectSocket, (char*)&bufferLength, (int) sizeof(bufferLength), 0);

    if (result == SOCKET_ERROR) { //socket error
        emit networkFailure();
        delete[] sendBuf;
        gData::socketMutex.unlock();
        return;
    }


    //sends message containing username and encrypted password to server
    result = send(gData::ConnectSocket, sendBuf, (int) bufferLength, 0);
    if (result == SOCKET_ERROR) { //socket error
        delete[] sendBuf;
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    int selectStatus = pollSocket();
    //time out occurred
    if (selectStatus <= 0) {
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    int status; //stores status from server
    for (result = 0; (result += recv(gData::ConnectSocket, (char *)&status, sizeof(status), 0)) < (int) sizeof(status);) {

        if (result == SOCKET_ERROR) { //socket error
            delete[] sendBuf;
            gData::socketMutex.unlock();
            emit networkFailure();
           return;
        }

        selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

    }

    gData::socketMutex.unlock(); //unlocks socket
    delete[] sendBuf;
    emit LogRegStatus(method, status);
}

//sets data from GUI
void LoginRegisterModule::setVariables(int bufferLength, char *sendBuf, int method) {
    this->bufferLength = bufferLength;
    this->sendBuf = sendBuf;
    this->method = method;
}

//polls socket for data
int LoginRegisterModule::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
