#ifndef JOINEXISTINGGAME_H
#define JOINEXISTINGGAME_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>


class joinExistingGame : public QThread {
    Q_OBJECT
public:
    void run();

    signals:
    void networkFailure();
    void joinGameStatus(char *userName, int wins, int loses, int ties);
private:
    int pollSocket();
};


#endif // JOINEXISTINGGAME_H

