#ifndef GLOBALDATA_H
#define GLOBALDATA_H
#include <winsock2.h>
#include <ws2tcpip.h>
#include <QMutex>

namespace gData{
enum moduleID {
    QUIT = -1,
    REGISTER_MODULE = 1,
    LOGIN_MODULE = 2,
    LIST_GAME_MODULE = 3,
    CREATE_GAME_MODULE = 4,
    SCOREBOARD_MODULE = 5,
    JOIN_GAME_MODULE = 6,
    LOGOUT_MODULE = 7,
    DELETE_MODULE = 8,
    HOST_POLL_MODULE = 9,
    KEEP_ALIVE_MODULE = 10,
    POLL_GAME_STATUS = 11,
    MAKE_MOVE_MODULE = 12,
    QUIT_GAME_MODULE = 13,
    SEND_MESSAGE_MODULE = 14,
    POLL_MESSAGE_MODULE = 15
};
    extern SOCKET ConnectSocket;
    extern char *port;
    extern char *ip;
    extern bool running;
    extern int numOfTopScores;
    extern bool findingGames;
    extern int numOfGamesListing;
    extern int timeOutFunc;
    extern QMutex socketMutex;
    extern int bufferSize;
    extern int MAX_CHARS;
    extern int gID;
    extern int whosTurn;
    extern int gameMaker;
    extern moduleID modID;
    extern bool loggedIn;
    extern QMutex boardLock;
    extern int scoreRefreshInt;
    extern int listGameRefresh;
    extern int *gameBoard;
    extern bool gameRunning;
    extern char *username;
    extern char *otherPlayer;
    extern int gameStatusPollInt;
    extern int KEEPALIVE_TIME;
    extern bool foundOtherPlayer;
    extern QMutex playerSearch;
    extern QMutex isRunning;
    extern int MSG_SIZE;
    extern int MSG_POLL_TIME;
}

#endif // GLOBALDATA_H
