#include "sendmove.h"
#include "globaldata.h"
//#include <windows.h>

void sendMove::run()
{
    //module on server to call
    int serverFuncID = gData::MAKE_MOVE_MODULE;

    gData::socketMutex.lock(); //locks socket

    int size = sizeof(serverFuncID)*3; //size of data to send server

    //sends size to server
    int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

    if (result == SOCKET_ERROR) { //socket error
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    //sends to server: (int module ID, int moveToSpot, int gameID)
    char *sendBuf = new char[size];
    sendBuf[0] = serverFuncID;
    sendBuf[sizeof(serverFuncID)] = spotMoved;
    sendBuf[sizeof(serverFuncID)*2] = gData::gID;
    result = send(gData::ConnectSocket, (char*)sendBuf,size, 0);

    if (result == SOCKET_ERROR) { //socket error
        gData::socketMutex.unlock();
        delete[] sendBuf;
        emit networkFailure();
        return;
    }
    delete[] sendBuf;

    int selectStatus = pollSocket();
    //time out occurred
    if (selectStatus <= 0) {
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    int status; //status from server of move command
    for (result = 0; (result += recv(gData::ConnectSocket, (char *)&status, sizeof(status), 0)) < (int) sizeof(status);) {

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
           return;
        }
        selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }
    }
    emit moveUpdated(status,  spotMoved); //lets main program know we got move response
    gData::socketMutex.unlock(); //unlocks socket
}

//sets spot to try to move to
void sendMove::setAction(int toDo){
    spotMoved = toDo;
}

//polls socket for data
int sendMove::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
