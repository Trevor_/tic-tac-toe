#ifndef POLLONCREATEDGAME_H
#define POLLONCREATEDGAME_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>


class pollOnCreatedGame : public QThread {
    Q_OBJECT
public:
    void run();

    signals:
    void networkFailure();
    void secondPlayerJoined(char *userName, int wins, int loses, int ties);
    void statusUpdate(bool);
private:
    int pollSocket();
};

#endif // POLLONCREATEDGAME_H
