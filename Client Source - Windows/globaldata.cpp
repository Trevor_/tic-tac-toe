#include "globaldata.h"

namespace gData{

    /*----------------USER CONFIGURABLE DATA------------------*/
    char *ip = "207.115.73.17";
    char *port = "8282";
    int numOfTopScores = 5;             //number of top scores to display
    int numOfGamesListing = 10;         //number of games to display when finding available games
    int timeOutFunc = 8;                //how long before it considers a network call timed out
    int scoreRefreshInt = 5; //was 6    //in seconds, how fast it refreshes the top scores
    int listGameRefresh = 3; // was 4   //in seconds, how fast it refreshes available games
    int gameStatusPollInt = 1;          //in seconds, how fast it refreshes the gameboard while in a game
    int KEEPALIVE_TIME = 4; //was 2     //in seconds, how fast it sends the server a keep alive message
    int MSG_SIZE = 201;                 //chat messages are hardcoded to 201 bytes
    int MSG_POLL_TIME = 1;              //in seconds, how fast it polls for messages
    /*-------------END OF USER CONFIGURABLE DATA---------------*/



    /*----------------SERVER DATA DO NOT EDIT------------------*/
    /*----------------SERVER DATA DO NOT EDIT------------------*/
    /*----------------SERVER DATA DO NOT EDIT------------------*/
    QMutex socketMutex;                 //used to lock the socket so only a single thread can talk to it at once
    SOCKET ConnectSocket = INVALID_SOCKET;
    bool foundOtherPlayer = true;
    bool running = true;                //if the program is connected to the server for keep alive mainly
    bool findingGames = false;          //controls the findingGame module
    int bufferSize = 500;               //maximum buffer size of data to send/recev
    int MAX_CHARS = 200;                //max character count for stuff
    int whosTurn = -1;                  //-2 = lost other player in game, -1 = not your turn, 0 = server processing your last turn, 1 = your turn
    int gameMaker = -1;                 //game creator = 1 (player 1)
    int gID;                            //stores the current gameID
    char *username;                     //stores player 1(our) username
    QMutex boardLock;                   //locks the board to prevent thread from updating the tictactoe board data
    bool loggedIn = false;              //if user is logged in to server or not
    bool gameRunning = false;           //if a game is current in session
    char *otherPlayer;                  //player 2(the other player's username)
    int *gameBoard;                     //the tictactoe gameboard size 9
    QMutex playerSearch;                // used so can leave game
    QMutex isRunning;                   //used to lock the bool
}
