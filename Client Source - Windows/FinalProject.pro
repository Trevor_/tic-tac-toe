#-------------------------------------------------
#
# Project created by QtCreator 2014-05-23T01:04:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FinalProject
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    networkSetup.cpp \
    loginregistermodule.cpp \
    globaldata.cpp \
    listscores.cpp \
    findgames.cpp \
    creategame.cpp \
    polloncreatedgame.cpp \
    gamepolling.cpp \
    joinexistinggame.cpp \
    logoutordelete.cpp \
    keepalive.cpp \
    sendmove.cpp \
    messagemodule.cpp \
    msgsender.cpp

HEADERS  += mainwindow.h \
    networkSetup.h \
    loginregistermodule.h \
    globaldata.h \
    listscores.h \
    findgames.h \
    creategame.h \
    polloncreatedgame.h \
    gamepolling.h \
    joinexistinggame.h \
    logoutordelete.h \
    keepalive.h \
    sendmove.h \
    messagemodule.h \
    msgsender.h

FORMS    += mainwindow.ui
    win32:LIBS += -lsetupapi \
                            -lws2_32

CONFIG += static


    QMAKE_LFLAGS = -static -static-libgcc

RESOURCES += \
    images.qrc
 RC_FILE = myapp.rc
