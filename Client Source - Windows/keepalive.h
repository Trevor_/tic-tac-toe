#ifndef KEEPALIVE_H
#define KEEPALIVE_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>
class keepAlive  : public QThread {
    Q_OBJECT
public:
    void run();

    signals:
    void networkFailure();
private:
    int pollSocket();
};

#endif // KEEPALIVE_H
