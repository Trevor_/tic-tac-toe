#ifndef CREATEGAME_H
#define CREATEGAME_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class createGame : public QThread {
    Q_OBJECT
public:
    void run();

    signals:
    void networkFailure();
    void createdGame(int gameid);
private:
    int pollSocket();
};

#endif // CREATEGAME_H
