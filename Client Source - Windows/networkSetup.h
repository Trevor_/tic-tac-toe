#ifndef THREAD_H
#define THREAD_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class networkSetup : public QThread {
Q_OBJECT
public:
    void run();

    signals:
    void networkStatus(int stat);
};
#endif // THREAD_H
