#ifndef LOGINREGISTERMODULE_H
#define LOGINREGISTERMODULE_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class LoginRegisterModule : public QThread {
    Q_OBJECT
    public:
        void run();
        void setVariables(int bufferLength, char *sendBuf, int method);

        signals:
        void LogRegStatus(int type, int stat);
        void networkFailure();
    private:
        int method;
        int bufferLength;
        char *sendBuf;
        int pollSocket();
};

#endif // LOGINREGISTERMODULE_H
