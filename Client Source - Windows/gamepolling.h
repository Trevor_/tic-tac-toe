#ifndef GAMEPOLLING_H
#define GAMEPOLLING_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class gamePolling : public QThread {
    Q_OBJECT
    public:
        void run();

        signals:
        void networkFailure();
        void updateGameBoard(int turn, int gameOver, int winType);
    private:
        int pollSocket();
};


#endif // GAMEPOLLING_H
