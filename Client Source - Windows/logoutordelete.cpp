#include "logoutordelete.h"
#include "globaldata.h"
#include <windows.h>

//runs delete/logout or quit game module
void logoutOrDelete::run()
{
    int serverFuncID;
    if (action ==  gData::DELETE_MODULE) {
        serverFuncID = gData::DELETE_MODULE;
    }
    else if (action == gData::LOGOUT_MODULE) {
        serverFuncID = gData::LOGOUT_MODULE;
    }
    else {
        serverFuncID = gData::QUIT_GAME_MODULE;
    }


    gData::socketMutex.lock(); //locks socket
    int size = sizeof(serverFuncID); //size of data to send

    //sends size to server
    int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

    if (result == SOCKET_ERROR) { //socket error
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    //sends function to server
    result = send(gData::ConnectSocket, (char*)&serverFuncID,(int) sizeof(serverFuncID), 0);

    if (result == SOCKET_ERROR) { //socket error
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    if (action == gData::QUIT_GAME_MODULE) { //quit game so need to let UI know
        gData::socketMutex.unlock();
        emit deleted(3);
        return;
    }
    else if (action == gData::QUIT) { //QUIT program
        closesocket(gData::ConnectSocket);
        gData::socketMutex.unlock();
        emit quitStatus();
        return;
    }
    else if (action == gData::DELETE_MODULE) { //delete user
        int selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        int status; //status of request from server
        for (result = 0; (result += recv(gData::ConnectSocket, (char *)&status, sizeof(status), 0)) < (int) sizeof(status);) {

            if (result == SOCKET_ERROR) { //socket error
                gData::socketMutex.unlock();
                emit networkFailure();
               return;
            }
            selectStatus = pollSocket();
            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }

        }
        emit deleted(status); //sends deleted info back to main program
    }
    gData::socketMutex.unlock();
}

//sets which of the actions to do (delete/logout/leave game)
void logoutOrDelete::setAction(int toDo){
    action = toDo;
}

//polls socket for data
int logoutOrDelete::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
