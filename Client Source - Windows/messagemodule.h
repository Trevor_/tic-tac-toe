#ifndef MESSAGEMODULE_H
#define MESSAGEMODULE_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class messengerModule : public QThread {
    Q_OBJECT
public:
    void run();

    signals:
    void networkFailure();
    void messageUpdate(char *newMsg);
private:
    int pollSocket();
};

#endif // MESSAGEMODULE_H
