import QtQuick 2.2
import QtQuick.Window 2.1

Window {
    visible: true
    width: 360
    height: 360

    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }
    }

    Text {
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }

    TextInput {
        id: textInput1
        x: -111
        y: 204
        width: 80
        height: 20
        text: qsTr("Text Input")
        font.pixelSize: 12
    }
}
