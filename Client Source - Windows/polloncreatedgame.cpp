#include "polloncreatedgame.h"
#include "globaldata.h"
#include <windows.h>


void pollOnCreatedGame::run()
{
    //function id to send server
    int serverFuncID = gData::HOST_POLL_MODULE;
    gData::foundOtherPlayer = false;

    //runs only when we havent found another player yet
    while (gData::foundOtherPlayer == false)
    {
        emit statusUpdate(true);
        gData::socketMutex.lock(); //locks socket
        //locks the player search afterwards to prevent bad lock holding
        gData::playerSearch.lock();
        size_t len = strlen(gData::username)+1; //size of username
        int totalData = sizeof(int)*3+len;

        //sends size of next message to server
        int result = send(gData::ConnectSocket, (char*)&totalData,(int) sizeof(totalData), 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            gData::playerSearch.unlock();
            emit networkFailure();
            return;
        }

        //sends toserver: (int moduleID, int gameID, int usernameLength, char *username)
        char *sendBuf = new char[gData::bufferSize];
        sendBuf[0] = serverFuncID;
        sendBuf[sizeof(serverFuncID)] = gData::gID;
        sendBuf[sizeof(serverFuncID)*2] = len;
        memcpy(sendBuf + sizeof(int)*3, gData::username, len);

        result = send(gData::ConnectSocket, sendBuf,(int) totalData, 0);
        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            gData::playerSearch.unlock();
            delete[] sendBuf;
            emit networkFailure();
            return;
        }

        delete[] sendBuf;
        int selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            gData::playerSearch.unlock();
            emit networkFailure();
            return;
        }

        int recvSize = 0; //size of data from server
        for (result = 0; (result += recv(gData::ConnectSocket, (char*)&recvSize, (int) sizeof(recvSize),0)) < (int) sizeof(recvSize) ;) {
            if (result == SOCKET_ERROR) { //socket error
                gData::socketMutex.unlock();
                gData::playerSearch.unlock();
                emit networkFailure();
               return;
            }
            selectStatus = pollSocket();
            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                gData::playerSearch.unlock();
                emit networkFailure();
                return;
            }

        }

        //no users found yet
        if (recvSize == -1 ) {
            gData::socketMutex.unlock();
            gData::playerSearch.unlock();
            emit statusUpdate(false);
            sleep(gData::gameStatusPollInt);
        }
        else {
            //found user
            selectStatus = pollSocket();
                    //time out occurred
                    if (selectStatus <= 0) {
                        gData::socketMutex.unlock();
                        gData::playerSearch.unlock();
                        emit networkFailure();
                        return;
                    }
            //gets data from server
            char *recvBuf = new char[gData::bufferSize];
            for (result = 0; (result += recv(gData::ConnectSocket, (char*)recvBuf, recvSize ,0)) < recvSize;) {
                if (result == SOCKET_ERROR) { //socket error
                    gData::socketMutex.unlock();
                    gData::playerSearch.unlock();
                    delete[] recvBuf;
                    emit networkFailure();
                    return;
                  }
                selectStatus = pollSocket();
                //time out occurred
                if (selectStatus <= 0) {
                    gData::socketMutex.unlock();
                    gData::playerSearch.unlock();
                    emit networkFailure();
                    return;
                }    
            }

            //gets from server (int usernameLength, username, int wins, int loses, int ties)
            int usernameLength = (int)recvBuf[0];
            int wins = recvBuf[sizeof(int)+usernameLength];
            int loses= recvBuf[sizeof(int)*2+usernameLength];
            int ties= recvBuf[sizeof(int)*3+usernameLength];
            char *username = new char[usernameLength];
            memcpy(username, recvBuf + sizeof(int), usernameLength);
            gData::foundOtherPlayer = true; //yep we found another player
            gData::socketMutex.unlock(); //unlocks socket
            gData::playerSearch.unlock(); //unlocks playersearch boolean
            //lets main app know we got info
            emit secondPlayerJoined(username, wins, loses, ties);
        }
    }
}

//polls socket for data
int pollOnCreatedGame::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
