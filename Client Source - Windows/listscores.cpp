#include "listscores.h"
#include "globaldata.h"
#include <windows.h>
void listScores::run()
{

    char *username[gData::numOfTopScores];
    int *wins[gData::numOfTopScores];
    int *loses[gData::numOfTopScores];
    int *ties[gData::numOfTopScores];
    //initialzes data to -1
    for (int i = 0; i < gData::numOfTopScores; i++) {
        wins[i] = new int(-1);
        loses[i] = new int(-1);
        ties[i] = new int(-1);
    }

    //module of the server to call
    int serverFuncID = gData::SCOREBOARD_MODULE;

    //only runs while logged in
    while(gData::loggedIn == true)
    {
        //initalizes data to -1
        for (int i = 0; i < gData::numOfTopScores; i++) {
            *wins[i] = -1;
            *loses[i] = -1;
            *ties[i] = -1;
        }

        //locks socket before sending
        gData::socketMutex.lock();

        //size of data to send server
        int size = sizeof(serverFuncID);

        //sends size to server
        int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        //send function ID of list score
        result = send(gData::ConnectSocket, (char*)&serverFuncID,(int) sizeof(serverFuncID), 0);
        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        int selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        int numOfAvailTop = 0; //number of top score available from server

        for (result = 0; (result += recv(gData::ConnectSocket, (char*)&numOfAvailTop, (int) sizeof(numOfAvailTop),0)) < (int)sizeof(numOfAvailTop);) {
            if (result == SOCKET_ERROR) { //socket error
                gData::socketMutex.unlock();
                emit networkFailure();
               return;
            }
            selectStatus = pollSocket();
            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }

        }

        if (numOfAvailTop == -1) { //no top scores available
            gData::socketMutex.unlock();
            emit updateScores(username, wins, loses, ties);
        }

        char dataBuf[gData::bufferSize]; //buffer to receive from server

        //for each available score, gets score info
        for (int i = 0; i < numOfAvailTop; i++)  {
            selectStatus = pollSocket();

            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }

            int length = 0;

            //gets length of score info
            for (result = 0; (result += recv(gData::ConnectSocket, (char *)&length, (int) sizeof(length),0)) < (int)sizeof(length);) {
                if (result == SOCKET_ERROR) { //socket error
                    gData::socketMutex.unlock();
                    emit networkFailure();
                   return;
                }
                selectStatus = pollSocket();
                //time out occurred
                if (selectStatus <= 0) {
                    gData::socketMutex.unlock();
                    emit networkFailure();
                    return;
                }

            }
            selectStatus = pollSocket();

            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }

            //gets score data itself
            for (result = 0; (result += recv(gData::ConnectSocket, (char*)&dataBuf, length,0)) < length;) {
                if (result == SOCKET_ERROR) { //socket error
                    gData::socketMutex.unlock();
                    emit networkFailure();
                   return;
                }
                selectStatus = pollSocket();
                //time out occurred
                if (selectStatus <= 0) {
                    gData::socketMutex.unlock();
                    emit networkFailure();
                    return;
                }

            }

            //data from server: (int username length, char *username, int wins, int loses, int ties)
            int usernameLength = (int)dataBuf[0];
            username[i] = new char[usernameLength];
            memcpy(username[i], &dataBuf[sizeof(int)], usernameLength);
            int temp = sizeof(int)+usernameLength;
            *wins[i] = (int)dataBuf[temp];
            temp += sizeof(int);
            *loses[i] = (int)dataBuf[temp];
            temp += sizeof(int);
            *ties[i] = (int)dataBuf[temp];
        }

        gData::socketMutex.unlock();

        //every <X> seconds get data from server and send it back
        emit updateScores(username, wins, loses, ties);
        sleep(gData::scoreRefreshInt);

    }
    return;
}

//polls socket for data
int listScores::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
        selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
