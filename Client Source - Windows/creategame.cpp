#include "creategame.h"
#include "globaldata.h"


void createGame::run()
{
    int serverFuncID = gData::CREATE_GAME_MODULE; //ID of module to call on server
    gData::socketMutex.lock();                     //locks socket
    int size = sizeof(serverFuncID);               //size of data to send


    //sends length of message
    int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

    if (result == SOCKET_ERROR) { //socket failure
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    //sends the actual message now
    result = send(gData::ConnectSocket, (char*)&serverFuncID,(int) sizeof(serverFuncID), 0);

    if (result == SOCKET_ERROR) { //socket failure
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    //checks for data to read in
    int selectStatus = pollSocket();
    //time out occurred
    if (selectStatus <= 0) {
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }


    int gameId = 0;
    //gets the gameID from server
    for (result = 0; (result += recv(gData::ConnectSocket, (char*)&gameId, (int) sizeof(gameId),0)) < (int)sizeof(gameId);) {

        if (result == SOCKET_ERROR) { //socket failure
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        //checks for data to read from server
        selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

    }
    gData::socketMutex.unlock(); //done with mutex


    //creating tic-tac-toe board
    int *gameBoard[9]; // 9 is size of a tic-tac-toe board
    for (int i = 0; i < 9; i++) {
        gameBoard[i] = new int(0);
        gData::gameBoard[i] = *gameBoard[i];
    }
    //calls gui to refresh
    emit createdGame(gameId);
}

//checks for data to read in
int createGame::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
