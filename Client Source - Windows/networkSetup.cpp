#include "networkSetup.h"
#include <globaldata.h>

void networkSetup::run() {

    gData::socketMutex.lock(); //locks socket
    WSADATA wsaData;

    int status = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (status != 0) { //could open up socket
        gData::socketMutex.unlock();
        emit networkStatus(-1);
        return;
    }
    struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;

    //zeros memory space
    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;


    status = getaddrinfo((const char*)gData::ip, gData::port, &hints, &result);
    if (status != 0) { //bad data
        gData::socketMutex.unlock();
        emit networkStatus(-1);
        return;
    }

    //loops through addresses until we have a successful hit
    for (ptr = result; ptr != NULL; ptr=ptr->ai_next) {
        // Create a SOCKET for connecting to server
        gData::ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
            ptr->ai_protocol);

       if (gData::ConnectSocket == INVALID_SOCKET) {
            //goes onto the next one if invalid
           continue;
        }

        // Connect to server.
        status = ::connect( gData::ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (status == SOCKET_ERROR) {
            closesocket(gData::ConnectSocket);
            continue;
        }
        break; //succesfully connected
    }

    //done with this
    freeaddrinfo(result);

    if (gData::ConnectSocket == INVALID_SOCKET) { //bad socket
        gData::socketMutex.unlock();
        emit networkStatus(-1);
        return;
    }

    gData::socketMutex.unlock();
    emit networkStatus(status); //lets main app know
    return;
}
