#include "keepalive.h"
#include "globaldata.h"
#include <windows.h>

void keepAlive::run()
{
    int serverFuncID = gData::KEEP_ALIVE_MODULE;
    while (gData::running == true) {

        gData::socketMutex.lock(); //locks socket
        int size = sizeof(serverFuncID); //size of data to send server

        //sends length of next msg to server
        int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        //sends keep alive to server
        result = send(gData::ConnectSocket, (char*)&serverFuncID,(int) sizeof(serverFuncID), 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }
        gData::socketMutex.unlock();
        //sleeps for keepalive time
        sleep(gData::KEEPALIVE_TIME);

    }
}
