#include "joinexistinggame.h"
#include "globaldata.h"
#include <windows.h>


void joinExistingGame::run()
{
    int serverFuncID = gData::JOIN_GAME_MODULE; //the module to call on the server side

    gData::socketMutex.lock(); //locks socket so no one else can read/write to it
    int totalData = sizeof(int)*2; //size of data to send to server

    //sends size to server
    int result = send(gData::ConnectSocket, (char*)&totalData,(int) sizeof(totalData), 0);

    //socket error
    if (result == SOCKET_ERROR) {
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    //sends (int moduleID, int gameID) to server
    char *sendBuf = new char[totalData];
    sendBuf[0] = serverFuncID;
    sendBuf[sizeof(serverFuncID)] = gData::gID;

    result = send(gData::ConnectSocket, sendBuf,(int) totalData, 0);
    if (result == SOCKET_ERROR) { //socket rror
        gData::socketMutex.unlock();
        delete[] sendBuf;
        emit networkFailure();
        return;
    }

    delete[] sendBuf;
    int selectStatus = pollSocket();

    //time out occurred
    if (selectStatus <= 0) {
        gData::socketMutex.unlock();
        emit networkFailure();
        return;
    }

    int recvSize = 0; //size of data getting from server

    //gets data from server
    for (result = 0; (result += recv(gData::ConnectSocket, (char*)&recvSize, (int) sizeof(recvSize),0)) < (int) sizeof(recvSize) ;) {
        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
           return;
        }
        selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

    }

    //if the data we get is -1 that means unable to join
    if (recvSize == -1 ) {
        gData::socketMutex.unlock();
        int wins = -1;
        int loses = -1;
        int ties = -1;
        char *username;
        emit joinGameStatus(username, wins, loses, ties);
        sleep(gData::POLL_GAME_STATUS);
    }
    else {
    //not -1 so we have joined the game successfully

        selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        //gets data from server
        char *recvBuf = new char[gData::bufferSize];
        for (result = 0; (result += recv(gData::ConnectSocket, (char*)recvBuf, recvSize ,0)) < recvSize;) {
            if (result == SOCKET_ERROR) { //socket error
                gData::socketMutex.unlock();
                delete[] recvBuf;
                emit networkFailure();
                return;
              }

            selectStatus = pollSocket();
            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }

        }

        //server sent (int usrname len, int wins, int loses, int ties)
        int usernameLength = (int)recvBuf[0];
        int wins = recvBuf[sizeof(int)+usernameLength];
        int loses= recvBuf[sizeof(int)*2+usernameLength];
        int ties = recvBuf[sizeof(int)*3+usernameLength];
        char *username = new char[usernameLength];
        std::cout << " username length: " << usernameLength << " wins " << wins << " loses: " << loses << " ties " << ties << std::endl;
        memcpy(username, recvBuf + sizeof(int), usernameLength);
        gData::socketMutex.unlock();
        emit joinGameStatus(username, wins, loses, ties); //calls GUI to handle it
}
}

//polls socket to see if it has information
int joinExistingGame::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
