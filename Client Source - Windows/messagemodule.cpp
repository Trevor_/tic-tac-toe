#include "messagemodule.h"
#include "globaldata.h"
#include <windows.h>

void messengerModule::run()
{
    //module to call on server
    int serverFuncID = gData::POLL_MESSAGE_MODULE;

    //runs while in a game
    while (gData::gameRunning == true)
    {

        gData::socketMutex.lock(); //locks socket from other threads

        int size = sizeof(serverFuncID); //size of data to send server
        int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        char *sendBuf = new char[size];
        sendBuf[0] = serverFuncID;
        result = send(gData::ConnectSocket, (char*)sendBuf,size, 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            delete[] sendBuf;
            emit networkFailure();
            return;
        }
        delete[] sendBuf;


        int selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        int status; //stores result from server
        for (result = 0; (result += recv(gData::ConnectSocket, (char *)&status, sizeof(status), 0)) < (int) sizeof(status);) {

            if (result == SOCKET_ERROR) { //socket error
                gData::socketMutex.unlock();
                emit networkFailure();
               return;
            }
            selectStatus = pollSocket();
            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }
        }

        if (status != -1) //no message for me if -1
        {
            char *newMsg = new char[gData::MSG_SIZE];
            memset(newMsg, 0, gData::MSG_SIZE);
            //gets message from server
            for (result = 0; (result += recv(gData::ConnectSocket, (char *)newMsg, gData::MSG_SIZE, 0)) < gData::MSG_SIZE;) {

                if (result == SOCKET_ERROR) { //socket error
                    gData::socketMutex.unlock();
                    emit networkFailure();
                   return;
                }
                selectStatus = pollSocket();
                //time out occurred
                if (selectStatus <= 0) {
                    gData::socketMutex.unlock();
                    emit networkFailure();
                    return;
                }
            }
            //returns message to main application
            emit messageUpdate(newMsg);
        }


        gData::socketMutex.unlock();
        //sleeps until <X> seconds before polling again
        sleep(gData::MSG_POLL_TIME);
    }
}

//polls socket for data
int messengerModule::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
