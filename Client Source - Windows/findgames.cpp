#include "findgames.h"
#include "globaldata.h"
#include <windows.h>

void findGames::run()
{

    //declaration of pointer arrays
    int *gameIds[gData::numOfGamesListing];
    char *username[gData::numOfGamesListing];
    int *wins[gData::numOfGamesListing];
    int *loses[gData::numOfGamesListing];
    int *ties[gData::numOfGamesListing];

    //initialzes to -1
    for (int i = 0; i < gData::numOfGamesListing; i++) {
        gameIds[i] = new int(-1);
        wins[i] = new int(-1);
        loses[i] = new int(-1);
        ties[i] = new int(-1);
    }

    int serverFuncID = gData::LIST_GAME_MODULE; //function to send to server

    //runs in loop to find game
    while(gData::findingGames == true)
    {
        //initialzes to -1
        for (int i = 0; i < gData::numOfGamesListing; i++) {
            *wins[i] = -1;
            *loses[i] = -1;
            *gameIds[i] = -1;
            *ties[i] = -1;
        }

        gData::socketMutex.lock(); //locks the socket so no one else can use it
        int size = sizeof(serverFuncID);

        //sends the size of the next message to the server
        int result = send(gData::ConnectSocket, (char*)&size,(int) sizeof(serverFuncID), 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        //sends the command to find the games to the server.
        result = send(gData::ConnectSocket, (char*)&serverFuncID,(int) sizeof(serverFuncID), 0);

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        int selectStatus = pollSocket();

        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        int numOfAvailGames = 0; //number of available games

        //gets number of available games from server
        for (result = 0; (result += recv(gData::ConnectSocket, (char*)&numOfAvailGames, (int) sizeof(numOfAvailGames),0)) < (int) sizeof(numOfAvailGames);) {
            if (result == SOCKET_ERROR) { //socket error
                gData::socketMutex.unlock();
                emit networkFailure();
               return;
            }
        }

        if (numOfAvailGames == -1) { //no available games if we get a -1
            gData::findingGames = false;
            gData::socketMutex.unlock();
            emit noGames();
            return;
        }

        char dataBuf[gData::bufferSize]; //creates a data buffer to store stuff

        //gets data for each available game from the server
        for (int i = 0; i < numOfAvailGames; i++)  {
            selectStatus = pollSocket();

            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }

            int length = 0;
            //gets length from server for <i> message from server
            for (result = 0; (result += recv(gData::ConnectSocket, (char *)&length, (int) sizeof(length),0)) < (int) sizeof(length);) {

                if (result == SOCKET_ERROR) { //socket error
                    gData::socketMutex.unlock();
                    emit networkFailure();
                   return;
                }

                selectStatus = pollSocket();
                //time out occurred
                if (selectStatus <= 0) {
                    gData::socketMutex.unlock();
                    emit networkFailure();
                    return;
                }
            }

            selectStatus = pollSocket();

            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }
            //gets the actual game data now
            for (result = 0; (result += recv(gData::ConnectSocket, (char*)&dataBuf, length,0)) < length;) {

                if (result == SOCKET_ERROR) { //socket error
                    gData::socketMutex.unlock();
                    emit networkFailure();
                   return;
                }

                 selectStatus = pollSocket();
                //time out occurred
                if (selectStatus <= 0) {
                    gData::socketMutex.unlock();
                    emit networkFailure();
                    return;
                }

            }

            //server sends: (int gameID, int username length, char *username, int wins, int loses, int ties)
            *gameIds[i] = (int) dataBuf[0];
            int usernameLength = (int)dataBuf[sizeof(int)];
            username[i] = new char[usernameLength];
            memcpy(username[i], &dataBuf[sizeof(int)*2], usernameLength);
            int temp = sizeof(int)*2+usernameLength;
            *wins[i] = (int)dataBuf[temp];
            temp += sizeof(int);
            *loses[i] = (int)dataBuf[temp];
            temp += sizeof(int);
            *ties[i] = (int)dataBuf[temp];
        }

        gData::socketMutex.unlock(); //unlocks socket
        emit updateAvailGames(gameIds, username, wins, loses, ties); //calls GUI to handle results
        //every x seconds get data from server and send it back
        sleep(gData::listGameRefresh);
    }
    return;
}

//polls socket for data
int findGames::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
