#ifndef MSGSENDER_H
#define MSGSENDER_H
#include <QThread>
#include <QMutex>
#include <winsock2.h>
#include <ws2tcpip.h>

class msgSender  : public QThread {
    Q_OBJECT
public:
    void run();
    void setMsgPtr(char *);

    signals:
    void networkFailure();
private:
    int pollSocket();
    char *msg;
};

#endif // MSGSENDER_H
