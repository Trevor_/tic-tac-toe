#include "gamepolling.h"
#include "globaldata.h"
#include <windows.h>

void gamePolling::run()
{
    //initalizes it to -1
    int turn = -1;
    int gameOver = -1;
    int winType = -1;

    int serverFuncID = gData::POLL_GAME_STATUS; //server function to send to server

    //loops while gamerunning is true and polls for game status
    while(gData::gameRunning == true)
    {
        int dataSize = sizeof(serverFuncID)*2; //size of data to send server
        gData::socketMutex.lock(); //locks socket

        //sends length of next message to server
        int result = send(gData::ConnectSocket, (char*)&dataSize,(int) sizeof(dataSize), 0);

        if (result == SOCKET_ERROR) {//socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        //send buffer of (int module, int gameID)
        char *sendBuf = new char[sizeof(int)*2];
        sendBuf[0] = serverFuncID;
        sendBuf[sizeof(int)] = gData::gID;

        //sends the actual message to server now
        result = send(gData::ConnectSocket, (char*)sendBuf, dataSize, 0);

        delete[] sendBuf;

        if (result == SOCKET_ERROR) { //socket error
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }



        int selectStatus = pollSocket();
        //time out occurred
        if (selectStatus <= 0) {
            gData::socketMutex.unlock();
            emit networkFailure();
            return;
        }

        int sizeRecvData = sizeof(int)*12; // fixed agreed upon size with server of data inbound to save a packet

        char *recvBuf = new char[sizeRecvData];

        //gets in latest game data
        for (result = 0; (result += recv(gData::ConnectSocket, (char*)recvBuf, sizeRecvData,0)) < sizeRecvData;) {
            if (result == SOCKET_ERROR) {
                gData::socketMutex.unlock();
                emit networkFailure();
               return;
            }
             selectStatus = pollSocket();
            //time out occurred
            if (selectStatus <= 0) {
                gData::socketMutex.unlock();
                emit networkFailure();
                return;
            }

        }
        //turn is stored at beginning
        //server data: (int turn, int gameboard[9], int gameOver, int winType)
        turn = (int)recvBuf[0];

        if (turn == -2) {  //then other play is gone!!
            delete[] recvBuf;
            gData::socketMutex.unlock();
            emit updateGameBoard(turn, gameOver, winType);
            return;
        }

        gData::boardLock.lock(); //locks the gameboard to update it, prevents GUI from updating

        //grabs gameboard data from buffer
        for (int i = 0; i < 9; i++) {
            gData::gameBoard[i] = (int)recvBuf[sizeof(int)+sizeof(int)*i];
         }

        gameOver = recvBuf[sizeof(int)*10];
        winType = recvBuf[sizeof(int)*11];
        delete[] recvBuf;


        gData::socketMutex.unlock(); //unlocks mutex

        if (gameOver != -1) { //game is over if it isnt -1
            gData::gameRunning = false;
        }

        emit updateGameBoard(turn, gameOver, winType); //calls GUI
        gData::boardLock.unlock(); //unlocks board
        //every x seconds get data from server and send it back
        sleep(gData::gameStatusPollInt);
    }
    return;
}

//polls socket for data
int gamePolling::pollSocket() {
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(gData::ConnectSocket, &fds);

    timeval tv;
    tv.tv_sec = gData::timeOutFunc;
    tv.tv_usec = 0;
    int selectStatus = 1;
    selectStatus = select(gData::ConnectSocket + 1, &fds, 0, 0, &tv);
    return selectStatus;
}
