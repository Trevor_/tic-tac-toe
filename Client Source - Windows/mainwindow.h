#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <globaldata.h>
#include <QMainWindow>
#include <QTableWidgetItem>
#include <string>

#include <winsock2.h>
#include <ws2tcpip.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    signals:

private slots:
    void on_loginButton_clicked();
    void on_registerButton_clicked();
    void onLoginRegisterChanged(int type, int status);
    void onNetworkStatusChanged(int stat);
    void onOpenNetwork(int stat);
    void onNetworkFailure();
    void onScore(char **username, int **wins, int **loses, int **ties);
    void onListGames(int **gameID, char **username, int **wins, int **loses, int **ties);
    void onCreatedGame(int gID);
    void on_listGameButton_clicked();
    void on_createGameButton_clicked();
    void on_goBackButton_clicked();
    void gotoPassword();
    void onGamePoller(char *userName, int wins, int loses, int ties);
    void onGamePollerStatus(bool);
    void onUpdateGameBoard(int turn, int gameOver, int winType);
    void on_actionChange_Server_triggered();
    void onUpdateJoinStatus(char *username, int wins, int loses, int ties);
    void on_sendChatBox_textChanged();
    void on_tictactoe_8_clicked();
    void on_sendMsgButton_clicked();
    void on_reconectButton_clicked();
    void on_tictactoe_0_clicked();
    void onUpdatedMove(int status, int move);
    void on_availGamesTable_doubleClicked(const QModelIndex &index);
    void on_deleteGameButton_clicked();
    void on_logOutButton_clicked();
    void onDeleteStatus(int deleted);
    void on_actionQuit_triggered();
    void onQuitStatus();
    void onNoGames();
    void on_tictactoe_1_clicked();
    void on_tictactoe_2_clicked();
    void on_tictactoe_3_clicked();
    void on_tictactoe_4_clicked();
    void on_tictactoe_5_clicked();
    void on_tictactoe_6_clicked();
    void on_tictactoe_7_clicked();
    void on_quitGameButton_clicked();
    void onMessage(char *message);

private:
    Ui::MainWindow *ui;
    std::vector<QTableWidgetItem *> tableUserName;
    std::vector<QTableWidgetItem *> tableWins;
    std::vector<QTableWidgetItem *> tableLoses;
    std::vector<QTableWidgetItem *> tableTies;
    std::vector<QTableWidgetItem *> availGameID;
    std::vector<QTableWidgetItem *> availGameUserName;
    std::vector<QTableWidgetItem *> availGameWins;
    std::vector<QTableWidgetItem *> availGameLoses;
    std::vector<QTableWidgetItem *> availGameTies;
    bool eventFilter(QObject *object, QEvent *event);
    void closeEvent (QCloseEvent *event);
    void boardHandler(int square);
    void networkFailure();
    void Register_Login(int);
    bool networkError;

};

#endif // MAINWINDOW_H
